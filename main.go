package main

import "gitlab.com/projectstolaunch/studentportal_microservice/api"

func main() {
	api.Run()
}