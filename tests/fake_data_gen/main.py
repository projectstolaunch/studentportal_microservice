import unittest
from FakeUser import FakeUser
from FakeCourse import FakeCourse
from FakeFile import FakeFile

class GenerateAllFake:
    fake_user_ids = []
    port = "4000"

    def generate_users(self):
        obj = FakeUser(self.port, 2,2,2,2)
        self.fake_user_ids = obj.generate()

    def generate_courses(self):
        terms_list = [[1, 2017], [5, 2017], [9, 2017], [1, 2018], [5, 2018], [9, 2018], [1, 2019], [5, 2019], [9, 2019]]
        obj = FakeCourse(self.port, self.fake_user_ids, 5, 4, 3, 3, terms_list)
        obj.generate()

    def generate_file(self):
        obj = FakeFile()
        obj.generate()

    def generate_all(self):
        self.generate_users()
        self.generate_courses()
        #self.generate_file()

obj = GenerateAllFake()
obj.generate_all()