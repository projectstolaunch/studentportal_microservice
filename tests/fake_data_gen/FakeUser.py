import requests
import json
import unittest
import traceback

class FakeUser(unittest.TestCase):

    def __init__(self, port, num_admins, num_students, num_tutors, num_student_tutors):
        super().__init__()
        self.num_admins = num_admins
        self.num_students = num_students
        self.num_tutors = num_tutors
        self.num_student_tutors = num_student_tutors

        self.fake_user_data_list = []
        self.fake_user_ids = []

        self.ctr = 0
        self.url = "http://localhost:" + port + "/users"
        self.headers = {'content-type': 'application/json'}
        return

    def create_fake_user_admins(self):
        for _ in range(self.num_admins):
            self.ctr += 1
            new_user = {
                "user_display_name": "user_" + str(self.ctr),
                "user_email": "user" + str(self.ctr) + "@gmail.com",
                "user_phone": "4444-999-10" + str(self.ctr),
                "password": "as",
                "is_admin": True,
                "is_student": False,
                "is_tutor": False,
            }
            self.fake_user_data_list.append(new_user)

    def create_fake_user_students(self):
        for _ in range(self.num_students):
            self.ctr += 1
            new_user = {
                "user_display_name": "user_" + str(self.ctr),
                "user_email": "user" + str(self.ctr) + "@gmail.com",
                "user_phone": "4444-999-10" + str(self.ctr),
                "password": "as",
                "is_admin": False,
                "is_student": True,
                "is_tutor": False,
            }
            self.fake_user_data_list.append(new_user)
    
    def create_fake_user_tutors(self):
        for _ in range(self.num_tutors):
            self.ctr += 1
            new_user = {
                "user_display_name": "user_" + str(self.ctr),
                "user_email": "user" + str(self.ctr) + "@gmail.com",
                "user_phone": "4444-999-10" + str(self.ctr),
                "password": "as",
                "is_admin": False,
                "is_student": False,
                "is_tutor": True,
            }
            self.fake_user_data_list.append(new_user)
    
    def create_fake_user_student_tutor(self):
        for _ in range(self.num_student_tutors):
            self.ctr += 1
            new_user = {
                "user_display_name": "user_" + str(self.ctr),
                "user_email": "user" + str(self.ctr) + "@gmail.com",
                "user_phone": "4444-999-10" + str(self.ctr),
                "password": "as",
                "is_admin": False,
                "is_student": True,
                "is_tutor": True,
            }
            self.fake_user_data_list.append(new_user)

    def create_fake_list(self):
        self.create_fake_user_admins()
        self.create_fake_user_students()
        self.create_fake_user_tutors()
        self.create_fake_user_student_tutor()
    
    def make_api_calls(self):
        sess = requests.Session()
        for fake_data in self.fake_user_data_list:
            resp = sess.post(self.url, data=json.dumps(fake_data), verify=False, headers=self.headers)
            try:
                resp_json = json.loads(resp.text)
                #resp_json = resp_json["user_info"]
                for key in fake_data:
                    if key == "password":
                        continue
                    self.assertEqual(resp_json[key], fake_data[key])
                if resp_json["is_admin"] == True or resp_json["is_student"] == True:
                    self.fake_user_ids.append(resp_json["user_id"])
            except Exception as e:
                print(resp.text, resp, e)
                print(traceback.format_exc())
        return
    
    def generate(self):
        self.create_fake_list()
        self.make_api_calls()
        return self.fake_user_ids