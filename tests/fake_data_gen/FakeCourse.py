import requests
import json
import unittest
import traceback

class FakeCourse(unittest.TestCase):

    def __init__(self, port, fake_user_ids, num_courses, num_programs, num_universities, num_locations, terms_list):
        super().__init__()
        self.fake_user_ids = fake_user_ids
        self.num_courses = num_courses
        self.num_programs = num_programs
        self.num_universities = num_universities
        self.num_locations = num_locations
        self.terms_list = terms_list
        self.port = port

        self.fake_course_data_list = []
        self.ctr = 0
        self.url = "http://localhost:" + port + "/courses"
        self.headers = {'content-type': 'application/json'}
        self.sess = requests.Session()
        return

    def create_fake_location(self, num_loc):
        url = "http://localhost:" + self.port + "/locations"
        fake_location = {}
        fake_location["street_number"] = num_loc
        fake_location["street_name"] = "street_" + str(num_loc)
        fake_location["city"] = "city_" + str(num_loc)
        fake_location["state_province"] = "state_" + str(num_loc)
        fake_location["country"] = "country_" + str(num_loc)
        fake_location["postal_code"] = "post_" + str(num_loc)

        resp = self.sess.post(url, data=json.dumps(fake_location), verify=False, headers=self.headers)
        print("*" * 60)
        print(resp, resp.text)
        print("*" * 60)
        resp_json = json.loads(resp.text)
        return resp_json["location_id"]

    def create_fake_university(self, num_uni, fake_location_id):
        url = "http://localhost:"+ self.port + "/universities"
        fake_uni = {}
        fake_uni["university_name"] = "Uni_" + str(num_uni)
        fake_uni["university_location_id"] = fake_location_id

        resp = self.sess.post(url, data=json.dumps(fake_uni), verify=False, headers=self.headers)
        resp_json = json.loads(resp.text)
        return resp_json["university_id"]

    def create_fake_program(self, num_prog, fake_uni_id):
        url = "http://localhost:" + self.port + "/programs"
        fake_prog = {}
        fake_prog["program_name"] = "Prog_" + str(num_prog)
        fake_prog["program_university_id"] = fake_uni_id

        resp = self.sess.post(url, data=json.dumps(fake_prog), verify=False, headers=self.headers)
        resp_json = json.loads(resp.text)
        return resp_json["program_id"]

    def create_fake_course(self, num_course, fake_prog_id):
        url = "http://localhost:" + self.port + "/courses"
        fake_course = {}
        fake_course["course_name"] = "course_name_" + str(num_course)
        fake_course["course_code"] = "CODE_" + str(num_course)
        fake_course["course_program_id"] = fake_prog_id

        resp = self.sess.post(url, data=json.dumps(fake_course), verify=False, headers=self.headers)
        resp_json = json.loads(resp.text)
        return resp_json["course_id"]

    def create_fake_terms(self, term_info, fake_course_id):
        url = "http://localhost:" + self.port + "/terms"
        fake_term = {}
        fake_term["term_start_month"] = term_info[0]
        fake_term["term_year"] = term_info[1]
        fake_term["term_course_id"] = fake_course_id

        resp = self.sess.post(url, data=json.dumps(fake_term), verify=False, headers=self.headers)
        resp_json = json.loads(resp.text)
        return resp_json["term_id"]
    
    def create_fake_tutorials(self, num_tutorial, fake_user_id, fake_course_id, fake_term_id):
        url = "http://localhost:" + self.port + "/tutorials"
        fake_term = {}
        fake_term["tutorial_title"] = "Tutorial_" + str(num_tutorial)
        fake_term["tutorial_link"] = "Tutorial_Link_" + str(num_tutorial)
        fake_term["tutorial_creator_id"] = fake_user_id
        fake_term["tutorial_course_id"] = fake_course_id
        fake_term["tutorial_term_id"] = fake_term_id

        resp = self.sess.post(url, data=json.dumps(fake_term), verify=False, headers=self.headers)
        resp_json = json.loads(resp.text)
        return resp_json["tutorial_id"]

    def create_fake_courses(self):
        fake_user_ctr = 0

        for num_loc in range(self.num_locations):
            fake_location_id = self.create_fake_location(num_loc + 1)

            for num_uni in range(self.num_universities):
                fake_uni_id = self.create_fake_university(num_uni + 1, fake_location_id)

                for num_prog in range(self.num_programs):
                    fake_prog_id = self.create_fake_program(num_prog + 1, fake_uni_id)

                    for num_course in range(self.num_courses):
                        fake_course_id = self.create_fake_course(num_course + 1, fake_prog_id)

                        for term_info in self.terms_list:
                            fake_term_id = self.create_fake_terms(term_info, fake_course_id)

                            for num_tutorial in range(2):
                                fake_tutorial_id = self.create_fake_tutorials(num_tutorial + 1, 
                                    self.fake_user_ids[fake_user_ctr], fake_course_id, fake_term_id)

                                fake_user_ctr += 1
                                if fake_user_ctr == len(self.fake_user_ids):
                                    fake_user_ctr = 0

        return
    

    def genCourses(self):
        for ctr in range(10):
            i = str(ctr + 1)
            data = {
                "city": "city_" + i,
                "province_state": "state_" + i,
                "country": "country_" + i,
                "university_name": "uni_" + i,
                "program_name": "prog_" + i,
                "course_name": "course_" + i,
                "course_code": "code_" + i
            }
            #print(json.dumps(data))
            resp = self.sess.get("http://localhost:4000/createcourse", params=(data), verify=False, headers=self.headers)
            resp_json = json.loads(resp.text)
            print(resp_json)


    def generate(self):
        self.genCourses()