import requests

class FakeFile:
    sess = requests.Session()
    #cmd = 'curl -X POST http://localhost:3000/files/1/1/upload -F "file=@F:/a.txt" -H "Content-Type: multipart/form-data"'

    def generate(self):
        headers = {'Content-Type': 'multipart/form-data',}
        url = 'http://localhost:3000/files/1/1/upload'
        files = {'file': ('F://a.txt', open('F://a.txt', 'rb')),}

        resp = self.sess.post(url, headers=headers, files=files, verify=False)
        print(resp, resp.text, resp.json)