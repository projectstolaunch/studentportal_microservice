import requests
import json
import unittest
from UserTestCase import UserTestCase

class LocationTestCase(unittest.TestCase):
    new_location = {
        "street_number": 123,
        "street_name": "street1",
        "city": "city1",
        "state_province": "state_province1",
        "country": "country1",
        "postal_code": "postal_code1",
    }

    url = "http://localhost:3000/locations"
    s = requests.Session()
    headers = {'content-type': 'application/json'}

    bearer_token = None
    created_loc_id = None

    def setUp(self):
        return

    def create_location(self):
        resp = self.s.post(self.url, data=json.dumps(self.new_location), verify=False, headers=self.headers)
        try:
            resp_json = json.loads(resp.text)
            for key in self.new_location:
                self.assertEqual(resp_json[key], self.new_location[key])
            self.created_loc_id = resp_json["location_id"]
        except Exception as e:
            print(resp, resp.text, resp.json, e)
        return

    def get_all_locations(self):
        resp = self.s.get(self.url, verify=False)
        resp_json = json.loads(resp.text)
        try:
            for key in self.new_location:
                self.assertEqual(resp_json[0][key], self.new_location[key])
        except Exception as e:
            print(resp_json, e)
        return

    def get_location_by_id(self):
        resp = self.s.get(self.url + "/" + str(self.created_loc_id), verify=False)
        resp_json = json.loads(resp.text)
        try:
            for key in self.new_location:
                self.assertEqual(resp_json[key], self.new_location[key])
        except Exception as e:
            print(resp_json, e)
        return

    def location_run_tests(self):
        self.create_location()
        self.get_all_locations()
        self.get_location_by_id()

    def delete_location(self):
        headers = {"Authorization": "Bearer " + self.bearer_token}
        resp = self.s.delete(self.url + "/" + str(self.created_loc_id), verify=False, headers=headers)
        self.assertEqual(resp.status_code, 204)
        return