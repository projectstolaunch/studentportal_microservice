import unittest
from LocationTestCase import LocationTestCase
from UserTestCase import UserTestCase
from UniversityTestCase import UniversityTestCase
from ProgramTestCase import ProgramTestCase
from CourseTestCase import CourseTestCase
from TutorialTestCase import TutorialTestCase
from TermTestCase import TermTestCase

def execution_sequence():
    utc = UserTestCase()
    utc.user_run_tests()

    ltc = LocationTestCase()
    ltc.bearer_token = utc.bearer_token
    ltc.location_run_tests()

    unitc = UniversityTestCase(ltc.created_loc_id, utc.bearer_token)
    unitc.university_test_cases()

    progtc = ProgramTestCase(unitc.created_uni_id, utc.bearer_token)
    progtc.program_test_cases()

    coursetc = CourseTestCase(progtc.created_prog_id, utc.bearer_token)
    coursetc.course_test_cases()

    tutorialtc = TutorialTestCase(utc.created_user_id, utc.bearer_token, coursetc.created_course_id)
    tutorialtc.tutorial_test_cases()
    
    termtc = TermTestCase(utc.created_user_id, utc.bearer_token, coursetc.created_course_id)
    termtc.term_test_cases()
    
    exit()
    termtc.delete_term()
    tutorialtc.delete_tutorial()
    coursetc.delete_course()
    progtc.delete_program()
    unitc.delete_university()
    ltc.delete_location()
    utc.delete_user()

if __name__ == '__main__':
    execution_sequence()