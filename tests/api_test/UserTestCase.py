import requests
import json
import unittest
import traceback

class UserTestCase(unittest.TestCase):
    new_user = {
        "user_display_name": "user1",
        "user_email": "user1@gmail.com",
        "user_phone": "4444-999-100",
        "password": "as",
        "is_admin": True,
        "is_student": True,
        "is_tutor": True,
    }

    login_data = {
        "user_email": "user1@gmail.com",
        "password": "as"
    }

    update_user_data = {
        "user_display_name": "user1",
        "user_email": "user1@gmail.com",
        "user_phone": "4444-888-100",
        "is_admin": True,
        "is_student": True,
        "is_tutor": True,
    }

    url = "http://localhost:3000/users"
    s = requests.Session()
    headers = {'content-type': 'application/json'}

    created_user_id = None
    bearer_token = None

    def setUp(self):
        return

    def create_user(self):
        resp = self.s.post(self.url, data=json.dumps(self.new_user), verify=False, headers=self.headers)
        resp_json = resp
        try:
            resp_json = json.loads(resp.text)
            for key in self.new_user:
                if key == "password":
                    continue
                self.assertEqual(resp_json[key], self.new_user[key])
            self.created_user_id = resp_json["user_id"]
        except Exception as e:
            print(resp.text, resp_json, e)
            print(traceback.format_exc())
        return

    def get_all_users(self):
        resp = self.s.get(self.url, verify=False)
        resp_json = resp
        try:
            resp_json = json.loads(resp.text)
            for key in self.new_user:
                if key == "password":
                    continue
                self.assertEqual(resp_json[0][key], self.new_user[key])
        except Exception as e:
            print(resp.text, resp_json, e)
            print(traceback.format_exc())
        return

    def get_user_by_id(self):
        resp = self.s.get(self.url + "/" + str(self.created_user_id), verify=False)
        resp_json = resp
        try:
            resp_json = json.loads(resp.text)
            for key in self.new_user:
                if key == "password":
                    continue
                self.assertEqual(resp_json[key], self.new_user[key])
        except Exception as e:
            print(resp_json, e)
        return

    def login(self):
        resp = self.s.post('http://localhost:3000/login', data=json.dumps(self.login_data), verify=False, headers=self.headers)
        self.assertGreaterEqual(len(resp.text), 10)
        #print(resp.text)
        self.bearer_token = resp.text[1:-1]
        #print(self.bearer_token)
        return

    def update_user(self):
        #self.bearer_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE1OTYxMDcwMjMsInVzZXJfaWQiOjF9.R0Npat2ak26wJDAelHeIrtfN8s94fSxU_QMvni_Nr-k"
        #self.created_user_id = 1
        #print(self.bearer_token)
        headers = {"Authorization": "Bearer " + self.bearer_token}
        resp = self.s.put(self.url + "/" + str(self.created_user_id), data=json.dumps(self.update_user_data), verify=False, headers=headers)
        resp_json = resp.text
        try:
            resp_json = json.loads(resp.text)
            for key in self.update_user_data:
                self.assertEqual(resp_json[key], self.update_user_data[key])
        except Exception as e:
            print(resp, resp_json, e)
            print(traceback.format_exc())
        return

    def get_user_by_id_updated(self):
        resp = self.s.get(self.url + "/" + str(self.created_user_id), verify=False)
        resp_json = json.loads(resp.text)
        try:
            self.assertEqual(resp_json["user_phone"], self.update_user_data["user_phone"])
        except Exception as e:
            print(resp_json, e)
        return

    def user_run_tests(self):
        self.create_user()
        self.get_all_users()
        self.get_user_by_id()
        self.login()
        self.update_user()
        self.get_user_by_id_updated()

    def delete_user(self):
        headers = {"Authorization": "Bearer " + self.bearer_token}
        resp = self.s.delete(self.url + "/" + str(self.created_user_id), verify=False, headers=headers)
        self.assertEqual(resp.status_code, 204)
        return