import requests
import json
import unittest

s = requests.Session()
headers = {'content-type': 'application/json'}

login_data = {
    "user_email": "a@a.com",
    "password": "a"
}
resp = s.post('http://localhost:4000/login', data=json.dumps(login_data), verify=False, headers=headers)
print(resp.json()["token"])

headers = {"Authorization": "Bearer " + resp.json()["token"]}

resp = s.post('http://localhost:4000/users/1/1', verify=False, headers=headers)
print(resp, resp.text)