import requests
import json
import unittest

class CourseTestCase(unittest.TestCase):

    def __init__(self, created_prog_id, bearer_token):
        super().__init__()
        self.new_course_data = {
            "course_name": "course_one",
            "course_code": "course_code_one",
            "course_program_id": created_prog_id,
        }

        self.update_course_data = {
            "course_name": "course_one_updated",
            "course_code": "course_code_one_updated",
            "course_program_id": created_prog_id,
        }

        self.url = "http://localhost:3000/courses"
        self.s = requests.Session()
        self.headers = {'content-type': 'application/json'}

        self.bearer_token = bearer_token
        self.created_course_id = None

    def setUp(self):
        return

    def create_course(self):
        resp = self.s.post(self.url, data=json.dumps(self.new_course_data), verify=False, headers=self.headers)
        resp_json = json.loads(resp.text)
        try:
            for key in self.new_course_data:
                self.assertEqual(resp_json[key], self.new_course_data[key])
            self.created_course_id = resp_json["course_id"]
        except Exception as e:
            print(resp_json, e)
        return

    def get_all_courses(self):
        resp = self.s.get(self.url, verify=False)
        resp_json = json.loads(resp.text)
        try:
            for key in self.new_course_data:
                self.assertEqual(resp_json[0][key], self.new_course_data[key])
        except Exception as e:
            print(resp_json, e)
        return

    def get_course_by_id(self):
        resp = self.s.get(self.url + "/" + str(self.created_course_id), verify=False)
        try:
            resp_json = json.loads(resp.text)
            for key in self.new_course_data:
                self.assertEqual(resp_json[key], self.new_course_data[key])
        except Exception as e:
            print(resp, resp.text, resp.json, e)
        return
    
    def update_course(self):
        headers = {"Authorization": "Bearer " + self.bearer_token}
        resp = self.s.put(self.url + "/" + str(self.created_course_id), data=json.dumps(self.update_course_data), verify=False, headers=headers)
        resp_json = json.loads(resp.text)
        try:
            for key in self.update_course_data:
                self.assertEqual(resp_json[key], self.update_course_data[key])
        except Exception as e:
            print(resp_json, e)
        return

    def course_test_cases(self):
        self.create_course()
        self.get_all_courses()
        self.get_course_by_id()
        self.update_course()

    def delete_course(self):
        headers = {"Authorization": "Bearer " + self.bearer_token}
        resp = self.s.delete(self.url + "/" + str(self.created_course_id), verify=False, headers=headers)
        self.assertEqual(int(resp.status_code), 204)
        return