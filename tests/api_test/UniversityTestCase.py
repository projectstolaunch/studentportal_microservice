import requests
import json
import unittest

class UniversityTestCase(unittest.TestCase):

    def __init__(self, created_loc_id, bearer_token):
        super().__init__()
        self.new_university_data = {
            "university_name": "uni_one",
            "university_location_id": created_loc_id,
        }

        self.update_university_data = {
            "university_name": "uni_one_updated",
            "university_location_id": created_loc_id,
        }

        self.url = "http://localhost:3000/universities"
        self.s = requests.Session()
        self.headers = {'content-type': 'application/json'}

        self.bearer_token = bearer_token
        self.created_uni_id = None


    def setUp(self):
        return

    def create_university(self):
        resp = self.s.post(self.url, data=json.dumps(self.new_university_data), verify=False, headers=self.headers)
        resp_json = json.loads(resp.text)
        try:
            for key in self.new_university_data:
                self.assertEqual(resp_json[key], self.new_university_data[key])
            self.created_uni_id = resp_json["university_id"]
        except Exception as e:
            print(resp_json, e)
        return

    def get_all_universities(self):
        resp = self.s.get(self.url, verify=False)
        resp_json = json.loads(resp.text)
        try:
            for key in self.new_university_data:
                self.assertEqual(resp_json[0][key], self.new_university_data[key])
        except Exception as e:
            print(resp_json, e)
        return

    def get_university_by_id(self):
        resp = self.s.get(self.url + "/" + str(self.created_uni_id), verify=False)
        resp_json = json.loads(resp.text)
        try:
            for key in self.new_university_data:
                self.assertEqual(resp_json[key], self.new_university_data[key])
        except Exception as e:
            print(resp_json, e)
        return
    
    def update_university(self):
        headers = {"Authorization": "Bearer " + self.bearer_token}
        resp = self.s.put(self.url + "/" + str(self.created_uni_id), data=json.dumps(self.update_university_data), verify=False, headers=headers)
        resp_json = json.loads(resp.text)
        try:
            for key in self.update_university_data:
                self.assertEqual(resp_json[key], self.update_university_data[key])
        except Exception as e:
            print(resp_json, e)
        return

    def university_test_cases(self):
        self.create_university()
        self.get_all_universities()
        self.get_university_by_id()
        self.update_university()

    def delete_university(self):
        headers = {"Authorization": "Bearer " + self.bearer_token}
        resp = self.s.delete(self.url + "/" + str(self.created_uni_id), verify=False, headers=headers)
        self.assertEqual(int(resp.status_code), 204)
        return