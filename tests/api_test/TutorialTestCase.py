import requests
import json
import unittest

class TutorialTestCase(unittest.TestCase):

    def __init__(self, created_user_id, bearer_token, created_course_id):
        super().__init__()
        self.new_tutorial_data = {
            "tutorial_title": "title_one",
            "tutorial_link": "link_one",
            "is_verified": False,
            "tutorial_creator_id": created_user_id,
            "tutorial_course_id": created_course_id,
        }

        self.update_tutorial_data = {
            "tutorial_title": "title_one_updated",
            "tutorial_link": "link_one_updated",
            "is_verified": True,
            "tutorial_creator_id": created_user_id,
            "tutorial_course_id": created_course_id,
        }

        self.url = "http://localhost:3000/tutorials"
        self.s = requests.Session()
        self.headers = {'content-type': 'application/json'}

        self.bearer_token = bearer_token
        self.created_tutorial_id = None


    def setUp(self):
        return

    def create_tutorial(self):
        headers = {"Authorization": "Bearer " + self.bearer_token}
        resp = self.s.post(self.url, data=json.dumps(self.new_tutorial_data), verify=False, headers=headers)
        resp_json = json.loads(resp.text)
        try:
            for key in self.new_tutorial_data:
                self.assertEqual(resp_json[key], self.new_tutorial_data[key])
            self.created_tutorial_id = resp_json["tutorial_id"]
        except Exception as e:
            print(resp_json, e)
        return

    def get_tutorial_by_id(self):
        resp = self.s.get(self.url + "/" + str(self.created_tutorial_id), verify=False)
        
        try:
            resp_json = json.loads(resp.text)
            for key in self.new_tutorial_data:
                self.assertEqual(resp_json[key], self.new_tutorial_data[key])
        except Exception as e:
            print(resp, resp.text, resp.json, e)
        return
    
    def update_tutorial(self):
        headers = {"Authorization": "Bearer " + self.bearer_token}
        resp = self.s.put(self.url + "/" + str(self.created_tutorial_id), data=json.dumps(self.update_tutorial_data), verify=False, headers=headers)
        resp_json = json.loads(resp.text)
        try:
            for key in self.update_tutorial_data:
                self.assertEqual(resp_json[key], self.update_tutorial_data[key])
        except Exception as e:
            print(resp_json, e)
        return

    def tutorial_test_cases(self):
        self.create_tutorial()
        self.get_tutorial_by_id()
        self.update_tutorial()

    def delete_tutorial(self):
        headers = {"Authorization": "Bearer " + self.bearer_token}
        resp = self.s.delete(self.url + "/" + str(self.created_tutorial_id), verify=False, headers=headers)
        self.assertEqual(int(resp.status_code), 204)
        return