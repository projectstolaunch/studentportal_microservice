import requests
import json
import unittest

class TermTestCase(unittest.TestCase):

    def __init__(self, created_user_id, bearer_token, created_course_id):
        super().__init__()
        self.new_term_data = {
            "term_start_month": 2,
            "term_year": 2020,
            "term_course_id": created_course_id,
        }

        self.url = "http://localhost:3000/terms"
        self.s = requests.Session()
        self.headers = {'content-type': 'application/json'}

        self.bearer_token = bearer_token
        self.created_term_id = None


    def setUp(self):
        return

    def create_term(self):
        headers = {"Authorization": "Bearer " + self.bearer_token}
        resp = self.s.post(self.url, data=json.dumps(self.new_term_data), verify=False, headers=headers)
        resp_json = json.loads(resp.text)
        try:
            for key in self.new_term_data:
                self.assertEqual(resp_json[key], self.new_term_data[key])
            self.created_term_id = resp_json["term_id"]
        except Exception as e:
            print(resp_json, e)
        return

    def get_term_by_id(self):
        resp = self.s.get(self.url + "/" + str(self.created_term_id), verify=False)
        
        try:
            resp_json = json.loads(resp.text)
            for key in self.new_term_data:
                self.assertEqual(resp_json[key], self.new_term_data[key])
        except Exception as e:
            print(resp, resp.text, resp.json, e)
        return

    def term_test_cases(self):
        self.create_term()
        self.get_term_by_id()

    def delete_term(self):
        headers = {"Authorization": "Bearer " + self.bearer_token}
        resp = self.s.delete(self.url + "/" + str(self.created_term_id), verify=False, headers=headers)
        self.assertEqual(int(resp.status_code), 204)
        return