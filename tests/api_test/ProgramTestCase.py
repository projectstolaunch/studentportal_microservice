import requests
import json
import unittest

class ProgramTestCase(unittest.TestCase):

    def __init__(self, created_uni_id, bearer_token):
        super().__init__()
        self.new_program_data = {
            "program_name": "prog_one",
            "program_university_id": created_uni_id,
        }

        self.update_program_data = {
            "program_name": "prog_one_updated",
            "program_university_id": created_uni_id,
        }

        self.url = "http://localhost:3000/programs"
        self.s = requests.Session()
        self.headers = {'content-type': 'application/json'}

        self.bearer_token = bearer_token
        self.created_prog_id = None

    def setUp(self):
        return

    def create_program(self):
        resp = self.s.post(self.url, data=json.dumps(self.new_program_data), verify=False, headers=self.headers)
        resp_json = json.loads(resp.text)
        try:
            for key in self.new_program_data:
                self.assertEqual(resp_json[key], self.new_program_data[key])
            self.created_prog_id = resp_json["program_id"]
        except Exception as e:
            print(resp_json, e)
        return

    def get_all_programs(self):
        resp = self.s.get(self.url, verify=False)
        resp_json = json.loads(resp.text)
        try:
            for key in self.new_program_data:
                self.assertEqual(resp_json[0][key], self.new_program_data[key])
        except Exception as e:
            print(resp_json, e)
        return

    def get_program_by_id(self):
        resp = self.s.get(self.url + "/" + str(self.created_prog_id), verify=False)
        try:
            resp_json = json.loads(resp.text)
            for key in self.new_program_data:
                self.assertEqual(resp_json[key], self.new_program_data[key])
        except Exception as e:
            print(resp, resp.text, resp.json, e)
        return
    
    def update_program(self):
        headers = {"Authorization": "Bearer " + self.bearer_token}
        resp = self.s.put(self.url + "/" + str(self.created_prog_id), data=json.dumps(self.update_program_data), verify=False, headers=headers)
        resp_json = json.loads(resp.text)
        try:
            for key in self.update_program_data:
                self.assertEqual(resp_json[key], self.update_program_data[key])
        except Exception as e:
            print(resp_json, e)
        return

    def program_test_cases(self):
        self.create_program()
        self.get_all_programs()
        self.get_program_by_id()
        self.update_program()

    def delete_program(self):
        headers = {"Authorization": "Bearer " + self.bearer_token}
        resp = self.s.delete(self.url + "/" + str(self.created_prog_id), verify=False, headers=headers)
        self.assertEqual(int(resp.status_code), 204)
        return