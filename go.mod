module gitlab.com/projectstolaunch/studentportal_microservice

go 1.14

require (
	github.com/0xAX/notificator v0.0.0-20191016112426-3962a5ea8da1 // indirect
	github.com/badoux/checkmail v0.0.0-20200623144435-f9f80cb795fa
	github.com/codegangsta/envy v0.0.0-20141216192214-4b78388c8ce4 // indirect
	github.com/codegangsta/gin v0.0.0-20171026143024-cafe2ce98974 // indirect
	github.com/cosmtrek/air v1.12.1 // indirect
	github.com/creack/pty v1.1.11 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fatih/color v1.9.0 // indirect
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/gorilla/mux v1.7.4
	github.com/imdario/mergo v0.3.10 // indirect
	github.com/jinzhu/gorm v1.9.14
	github.com/joho/godotenv v1.3.0
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/mattn/go-shellwords v1.0.10 // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/sys v0.0.0-20200724161237-0e2f3a69832c // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1
	gopkg.in/urfave/cli.v1 v1.20.0 // indirect
)
