package seed

import (
	"log"

	"github.com/jinzhu/gorm"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/models"
)

var users = []models.User{
	models.User{
		Nickname: "Steven victor",
		Email:    "steven@gmail.com",
		Password: "password",
		Phone:    "4444-999-090",
	},
	models.User{
		Nickname: "Martin Luther",
		Email:    "luther@gmail.com",
		Password: "password",
		Phone:    "4444-999-101",
	},
}

//Load function to load seed data in database
func Load(db *gorm.DB) {
	var err error

	// err = db.Debug().DropTableIfExists(&models.File{}, &models.Term{}, &models.Tutorial{}, &models.Course{}, &models.Program{},
	// 	&models.University{}, &models.Location{}, &models.User{}).Error
	// if err != nil {
	// 	log.Fatalf("cannot drop table: %v", err)
	// }
	// err = db.Debug().AutoMigrate(&models.User{}, &models.Location{}, &models.University{},
	// 	&models.Program{}, &models.Course{}, &models.Tutorial{}, &models.Term{}, &models.File{}).Error
	// if err != nil {
	// 	log.Fatalf("cannot migrate table: %v", err)
	// }

	err = db.Debug().Model(&models.University{}).AddForeignKey("university_location_id", "locations(id)", "cascade", "cascade").Error
	if err != nil {
		log.Fatalf("attaching foreign key error: %v", err)
	}

	err = db.Debug().Model(&models.Program{}).AddForeignKey("program_university_id", "universities(id)", "cascade", "cascade").Error
	if err != nil {
		log.Fatalf("attaching foreign key error: %v", err)
	}

	err = db.Debug().Model(&models.Course{}).AddForeignKey("course_program_id", "programs(id)", "cascade", "cascade").Error
	if err != nil {
		log.Fatalf("attaching foreign key error: %v", err)
	}

	err = db.Debug().Model(&models.Tutorial{}).AddForeignKey("tutorial_creator_id", "users(id)", "cascade", "cascade").Error
	if err != nil {
		log.Fatalf("attaching foreign key error: %v", err)
	}

	err = db.Debug().Model(&models.Tutorial{}).AddForeignKey("tutorial_course_id", "courses(id)", "cascade", "cascade").Error
	if err != nil {
		log.Fatalf("attaching foreign key error: %v", err)
	}

	err = db.Debug().Model(&models.Term{}).AddForeignKey("term_course_id", "courses(id)", "cascade", "cascade").Error
	if err != nil {
		log.Fatalf("attaching foreign key error: %v", err)
	}

	err = db.Debug().Model(&models.File{}).AddForeignKey("file_course_id", "courses(id)", "cascade", "cascade").Error
	if err != nil {
		log.Fatalf("attaching foreign key error: %v", err)
	}

	err = db.Debug().Model(&models.File{}).AddForeignKey("file_term_id", "terms(id)", "cascade", "cascade").Error
	if err != nil {
		log.Fatalf("attaching foreign key error: %v", err)
	}

	/*
		for i, _ := range users {
			err = db.Debug().Model(&models.User{}).Create(&users[i]).Error
			if err != nil {
				log.Fatalf("cannot seed users table: %v", err)
			}
			posts[i].AuthorID = users[i].ID

			err = db.Debug().Model(&models.Post{}).Create(&posts[i]).Error
			if err != nil {
				log.Fatalf("cannot seed posts table: %v", err)
			}
		}
	*/
}
