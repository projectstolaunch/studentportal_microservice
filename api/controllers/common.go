package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/auth"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/models"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/utils/formaterror"
)

//ModelType constant for model types
type ModelType uint16

//User model type
const (
	ModelTypeUser ModelType = iota + 1
	ModelTypeLocation
	ModelTypeUniversity
	ModelTypeProgram
	ModelTypeCourse
	ModelTypeFile
	ModelTypeTutorial
	ModelTypeTerm
)

type _modelPtrs struct {
	user       *models.User
	location   *models.Location
	university *models.University
	program    *models.Program
	course     *models.Course
	file       *models.File
	tutorial   *models.Tutorial
	term       *models.Term
}

type _modelArrayPtrs struct {
	users        *[]models.User
	locations    *[]models.Location
	universities *[]models.University
	programs     *[]models.Program
	courses      *[]models.Course
	files        *[]models.File
	tutorials    *[]models.Tutorial
	terms        *[]models.Term
}

/****************************************************************************************************/
/***************************************  CRUD FUNCTIONS  *******************************************/
/****************************************************************************************************/

func (server *Server) _unmarshalToModel(c *gin.Context, modelType ModelType, objModelPtr *_modelPtrs) error {

	var err error

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		log.Println("Read from body failed", err)
		c.JSON(http.StatusUnprocessableEntity, err)
		return err
	}

	if modelType == ModelTypeUser {
		err = json.Unmarshal(body, &(objModelPtr.user))
	} else if modelType == ModelTypeLocation {
		err = json.Unmarshal(body, &(objModelPtr.location))
	} else if modelType == ModelTypeUniversity {
		err = json.Unmarshal(body, &(objModelPtr.university))
	} else if modelType == ModelTypeProgram {
		err = json.Unmarshal(body, &(objModelPtr.program))
	} else if modelType == ModelTypeCourse {
		err = json.Unmarshal(body, &(objModelPtr.course))
	} else if modelType == ModelTypeFile {
		err = json.Unmarshal(body, &(objModelPtr.file))
	} else if modelType == ModelTypeTutorial {
		err = json.Unmarshal(body, &(objModelPtr.tutorial))
	} else if modelType == ModelTypeTerm {
		err = json.Unmarshal(body, &(objModelPtr.term))
	}

	if err != nil {
		log.Println("Json unmarshal failed\n", string(body), "\n", err)
		c.JSON(http.StatusUnprocessableEntity, err)
		return err
	}

	return nil
}

func (server *Server) _prepareValidateModel(c *gin.Context, modelType ModelType, objModelPtr *_modelPtrs) error {
	var err error

	if modelType == ModelTypeUser {
		objModelPtr.user.Prepare()
		err = objModelPtr.user.Validate("")
	} else if modelType == ModelTypeLocation {
		objModelPtr.location.Prepare()
		err = objModelPtr.location.Validate()
	} else if modelType == ModelTypeUniversity {
		objModelPtr.university.Prepare()
		err = objModelPtr.university.Validate()
	} else if modelType == ModelTypeProgram {
		objModelPtr.program.Prepare()
		err = objModelPtr.program.Validate()
	} else if modelType == ModelTypeCourse {
		objModelPtr.course.Prepare()
		err = objModelPtr.course.Validate()
	} else if modelType == ModelTypeFile {
		// TBD
	} else if modelType == ModelTypeTutorial {
		objModelPtr.tutorial.Prepare()
		err = objModelPtr.tutorial.Validate()
	} else if modelType == ModelTypeTerm {
		objModelPtr.term.Prepare()
		err = objModelPtr.term.Validate()
	}

	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, err)
		log.Println(err)
		return err
	}

	return nil
}

func (server *Server) _saveModel(c *gin.Context, modelType ModelType, objModelPtr *_modelPtrs) error {
	var err error

	if modelType == ModelTypeUser {
		objModelPtr.user, err = objModelPtr.user.Save(server.DB)
	} else if modelType == ModelTypeLocation {
		objModelPtr.location, err = objModelPtr.location.Save(server.DB)
	} else if modelType == ModelTypeUniversity {
		objModelPtr.university, err = objModelPtr.university.Save(server.DB)
	} else if modelType == ModelTypeProgram {
		objModelPtr.program, err = objModelPtr.program.Save(server.DB)
	} else if modelType == ModelTypeCourse {
		objModelPtr.course, err = objModelPtr.course.Save(server.DB)
	} else if modelType == ModelTypeFile {
		// file, err = file.Save(server.DB)
	} else if modelType == ModelTypeTutorial {
		objModelPtr.tutorial, err = objModelPtr.tutorial.Save(server.DB)
	} else if modelType == ModelTypeTerm {
		objModelPtr.term, err = objModelPtr.term.Save(server.DB)
	}

	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		c.JSON(http.StatusInternalServerError, formattedError)
		log.Println(err)
		return err
	}

	return nil
}

func (server *Server) _updateModel(c *gin.Context, modelType ModelType, objModelPtr *_modelPtrs) error {
	var err error

	objID, err := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return err
	}

	if modelType == ModelTypeUser {
		objModelPtr.user, err = objModelPtr.user.Update(server.DB, uint32(objID))
	} else if modelType == ModelTypeLocation {
		// objModelPtr.location, err = objModelPtr.location.Update(server.DB)
	} else if modelType == ModelTypeUniversity {
		objModelPtr.university, err = objModelPtr.university.Update(server.DB)
	} else if modelType == ModelTypeProgram {
		objModelPtr.program, err = objModelPtr.program.Update(server.DB)
	} else if modelType == ModelTypeCourse {
		objModelPtr.course, err = objModelPtr.course.Update(server.DB)
	} else if modelType == ModelTypeFile {
		// file, err = file.Update(server.DB)
	} else if modelType == ModelTypeTutorial {
		objModelPtr.tutorial, err = objModelPtr.tutorial.Update(server.DB)
	}

	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		c.JSON(http.StatusInternalServerError, formattedError)
		return err
	}

	return nil
}

func (server *Server) _deleteModel(c *gin.Context, modelType ModelType, objModelPtr *_modelPtrs) (uint64, error) {
	var err error

	objID, err := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return 0, err
	}

	if modelType == ModelTypeUser {
		_, err = objModelPtr.user.Delete(server.DB, uint32(objID))
	} else if modelType == ModelTypeLocation {
		_, err = objModelPtr.location.Delete(server.DB, objID)
	} else if modelType == ModelTypeUniversity {
		_, err = objModelPtr.university.Delete(server.DB, objID)
	} else if modelType == ModelTypeProgram {
		_, err = objModelPtr.program.Delete(server.DB, objID)
	} else if modelType == ModelTypeCourse {
		_, err = objModelPtr.course.Delete(server.DB, objID)
	} else if modelType == ModelTypeFile {
		// _, err = objModelPtr.file.Delete(server.DB, objID)
	} else if modelType == ModelTypeTutorial {
		_, err = objModelPtr.tutorial.Delete(server.DB, objID)
	} else if modelType == ModelTypeTerm {
		_, err = objModelPtr.term.Delete(server.DB, objID)
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return objID, err
	}

	return objID, err
}

/****************************************************************************************************/
/***************************************  SET RESPONSE FUNCTIONS  ***********************************/
/****************************************************************************************************/

func (server *Server) _setLocationHeaderAndJSONRespWithModelStatusCreated(c *gin.Context, modelType ModelType, objModelPtr *_modelPtrs) {
	if modelType == ModelTypeUser {
		c.Header("Location", fmt.Sprintf("%s%s/%d", c.Request.Host, c.Request.RequestURI, objModelPtr.user.ID))
		c.JSON(http.StatusCreated, objModelPtr.user)
	} else if modelType == ModelTypeLocation {
		c.Header("Location", fmt.Sprintf("%s%s/%d", c.Request.Host, c.Request.RequestURI, objModelPtr.location.ID))
		c.JSON(http.StatusCreated, objModelPtr.location)
	} else if modelType == ModelTypeUniversity {
		c.Header("Location", fmt.Sprintf("%s%s/%d", c.Request.Host, c.Request.RequestURI, objModelPtr.university.ID))
		c.JSON(http.StatusCreated, objModelPtr.university)
	} else if modelType == ModelTypeProgram {
		c.Header("Location", fmt.Sprintf("%s%s/%d", c.Request.Host, c.Request.RequestURI, objModelPtr.program.ID))
		c.JSON(http.StatusCreated, objModelPtr.program)
	} else if modelType == ModelTypeCourse {
		c.Header("Location", fmt.Sprintf("%s%s/%d", c.Request.Host, c.Request.RequestURI, objModelPtr.course.ID))
		c.JSON(http.StatusCreated, objModelPtr.course)
	} else if modelType == ModelTypeFile {
		// c.Header("Location", fmt.Sprintf("%s%s/%d", c.Request.Host, c.Request.RequestURI, file.ID))
		// c.JSON(http.StatusCreated, file)
	} else if modelType == ModelTypeTutorial {
		c.Header("Location", fmt.Sprintf("%s%s/%d", c.Request.Host, c.Request.RequestURI, objModelPtr.tutorial.ID))
		c.JSON(http.StatusCreated, objModelPtr.tutorial)
	} else if modelType == ModelTypeTerm {
		// log.Println("*************************")
		// log.Println(objModelPtr.term)
		// log.Println(http.StatusCreated)
		// log.Println(c)
		c.Header("Location", fmt.Sprintf("%s%s/%d", c.Request.Host, c.Request.RequestURI, objModelPtr.term.ID))
		// log.Println("*************************")
		// log.Println(objModelPtr.term)
		// log.Println(http.StatusCreated)
		// log.Println(c)
		c.JSON(http.StatusCreated, objModelPtr.term)
	}
}

func (server *Server) _setJSONRespWithModelArrayStatusOk(c *gin.Context, modelType ModelType, objModelArrPtr *_modelArrayPtrs) {
	if modelType == ModelTypeUser {
		c.JSON(http.StatusOK, objModelArrPtr.users)
	} else if modelType == ModelTypeLocation {
		c.JSON(http.StatusOK, objModelArrPtr.locations)
	} else if modelType == ModelTypeUniversity {
		c.JSON(http.StatusOK, objModelArrPtr.universities)
	} else if modelType == ModelTypeProgram {
		c.JSON(http.StatusOK, objModelArrPtr.programs)
	} else if modelType == ModelTypeCourse {
		c.JSON(http.StatusOK, objModelArrPtr.courses)
	} else if modelType == ModelTypeFile {
		// c.JSON(http.StatusOK, file)
	} else if modelType == ModelTypeTutorial {
		c.JSON(http.StatusOK, objModelArrPtr.tutorials)
	} else if modelType == ModelTypeTerm {
		c.JSON(http.StatusOK, objModelArrPtr.terms)
	}
}

func (server *Server) _setJSONRespWithModelStatusOk(c *gin.Context, modelType ModelType, objModelPtr *_modelPtrs) {
	if modelType == ModelTypeUser {
		c.JSON(http.StatusOK, objModelPtr.user)
	} else if modelType == ModelTypeLocation {
		c.JSON(http.StatusOK, objModelPtr.location)
	} else if modelType == ModelTypeUniversity {
		c.JSON(http.StatusOK, objModelPtr.university)
	} else if modelType == ModelTypeProgram {
		c.JSON(http.StatusOK, objModelPtr.program)
	} else if modelType == ModelTypeCourse {
		c.JSON(http.StatusOK, objModelPtr.course)
	} else if modelType == ModelTypeFile {
		// c.JSON(http.StatusOK, file)
	} else if modelType == ModelTypeTutorial {
		c.JSON(http.StatusOK, objModelPtr.tutorial)
	} else if modelType == ModelTypeTerm {
		c.JSON(http.StatusOK, objModelPtr.term)
	}
}

/****************************************************************************************************/
/***************************************  FIND FUNCTIONS  *******************************************/
/****************************************************************************************************/

func (server *Server) _modelFindAllAndPutInArr(c *gin.Context, modelType ModelType, objModelPtr *_modelPtrs,
	objModelArrPtr *_modelArrayPtrs) error {

	var err error

	if modelType == ModelTypeUser {
		objModelArrPtr.users, err = objModelPtr.user.FindAll(server.DB)
	} else if modelType == ModelTypeLocation {
		objModelArrPtr.locations, err = objModelPtr.location.FindAll(server.DB)
	} else if modelType == ModelTypeUniversity {
		objModelArrPtr.universities, err = objModelPtr.university.FindAll(server.DB)
	} else if modelType == ModelTypeProgram {
		objModelArrPtr.programs, err = objModelPtr.program.FindAll(server.DB)
	} else if modelType == ModelTypeCourse {
		objModelArrPtr.courses, err = objModelPtr.course.FindAll(server.DB)
	} else if modelType == ModelTypeFile {
		// files, err = objModelPtr.file.FindAll(server.DB)
	} else if modelType == ModelTypeTutorial {
		// tutorials, err = objModelPtr.tutorial.FindAll(server.DB)
	} else if modelType == ModelTypeTerm {
		objModelArrPtr.terms, err = objModelPtr.term.FindAll(server.DB)
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return err
	}
	return nil
}

func (server *Server) _modelFindRecentAndPutInArr(c *gin.Context, modelType ModelType, objModelPtr *_modelPtrs,
	objModelArrPtr *_modelArrayPtrs) error {

	var err error

	if modelType == ModelTypeUser {
		objModelArrPtr.users, err = objModelPtr.user.FindRecent(server.DB)
	} else if modelType == ModelTypeLocation {
		objModelArrPtr.locations, err = objModelPtr.location.FindRecent(server.DB)
	} else if modelType == ModelTypeUniversity {
		objModelArrPtr.universities, err = objModelPtr.university.FindRecent(server.DB)
	} else if modelType == ModelTypeProgram {
		objModelArrPtr.programs, err = objModelPtr.program.FindRecent(server.DB)
	} else if modelType == ModelTypeCourse {
		objModelArrPtr.courses, err = objModelPtr.course.FindRecent(server.DB)
	} else if modelType == ModelTypeFile {
		// files, err = objModelPtr.file.FindRecent(server.DB)
	} else if modelType == ModelTypeTutorial {
		// tutorials, err = objModelPtr.tutorial.FindRecent(server.DB)
	} else if modelType == ModelTypeTerm {
		objModelArrPtr.terms, err = objModelPtr.term.FindRecent(server.DB)
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return err
	}
	return nil
}

func (server *Server) _modelFindByID(c *gin.Context, modelType ModelType, objModelPtr *_modelPtrs) error {
	var err error

	objID, err := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return err
	}

	if modelType == ModelTypeUser {
		objModelPtr.user, err = objModelPtr.user.FindByID(server.DB, uint32(objID))
	} else if modelType == ModelTypeLocation {
		objModelPtr.location, err = objModelPtr.location.FindByID(server.DB, uint32(objID))
	} else if modelType == ModelTypeUniversity {
		objModelPtr.university, err = objModelPtr.university.FindByID(server.DB, uint32(objID))
	} else if modelType == ModelTypeProgram {
		objModelPtr.program, err = objModelPtr.program.FindByID(server.DB, uint32(objID))
	} else if modelType == ModelTypeCourse {
		objModelPtr.course, err = objModelPtr.course.FindByID(server.DB, uint32(objID))
	} else if modelType == ModelTypeFile {
		// objModelPtr.file, err = objModelPtr.file.FindByID(server.DB, uint32(objID))
	} else if modelType == ModelTypeTutorial {
		objModelPtr.tutorial, err = objModelPtr.tutorial.FindByID(server.DB, uint32(objID))
	} else if modelType == ModelTypeTerm {
		objModelPtr.term, err = objModelPtr.term.FindByID(server.DB, uint32(objID))
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return err
	}

	return nil
}

/****************************************************************************************************/
/***************************************  MODEL FUNCTIONS  ******************************************/
/****************************************************************************************************/

//Create creates model object
func (server *Server) Create(c *gin.Context, modelType ModelType) {
	var objModelPtr _modelPtrs
	var err error

	/*Unmarshal body into model*/
	err = server._unmarshalToModel(c, modelType, &objModelPtr)
	if err != nil {
		log.Println("Create: Unmarshal to Model failed")
		return
	}

	/*Prepare and Validate*/
	err = server._prepareValidateModel(c, modelType, &objModelPtr)
	if err != nil {
		log.Println("Create: _prepareValidateModel failed")
		return
	}

	/*Save Model*/
	err = server._saveModel(c, modelType, &objModelPtr)
	if err != nil {
		log.Println("Create: _saveModel failed")
		return
	}

	/*Set header and return response*/
	server._setLocationHeaderAndJSONRespWithModelStatusCreated(c, modelType, &objModelPtr)
}

//GetAll gets all model objects
func (server *Server) GetAll(c *gin.Context, modelType ModelType) {
	var objModelPtr _modelPtrs
	var objModelArrPtr _modelArrayPtrs
	var err error

	err = server._modelFindAllAndPutInArr(c, modelType, &objModelPtr, &objModelArrPtr)
	if err != nil {
		return
	}

	server._setJSONRespWithModelArrayStatusOk(c, modelType, &objModelArrPtr)
}

//GetRecent gets 10 recent records of model
func (server *Server) GetRecent(c *gin.Context, modelType ModelType) {
	var objModelPtr _modelPtrs
	var objModelArrPtr _modelArrayPtrs
	var err error

	err = server._modelFindRecentAndPutInArr(c, modelType, &objModelPtr, &objModelArrPtr)
	if err != nil {
		return
	}

	server._setJSONRespWithModelArrayStatusOk(c, modelType, &objModelArrPtr)
}

//GetByID gets a user by id
func (server *Server) GetByID(c *gin.Context, modelType ModelType) {
	var objModelPtr _modelPtrs
	var err error

	/* Find by ID */
	err = server._modelFindByID(c, modelType, &objModelPtr)
	if err != nil {
		return
	}

	server._setJSONRespWithModelStatusOk(c, modelType, &objModelPtr)
}

//Update updates the user
func (server *Server) Update(c *gin.Context, modelType ModelType) {
	var objModelPtr _modelPtrs
	var err error

	/* Authorize User */
	userID, err := auth.ExtractTokenID(c.Request)
	if err != nil {
		log.Println("Invalid token in update: ", userID)
		log.Println("Update", err)
		c.JSON(http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	/* Find by ID */
	err = server._modelFindByID(c, modelType, &objModelPtr)
	if err != nil {
		return
	}

	/* Check if user id from token matches with the user account being updated exists */
	if modelType == ModelTypeUser {
		if userID != uint32(objModelPtr.user.ID) {
			c.JSON(http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
			return
		}
	}

	/*Unmarshal body into model*/
	err = server._unmarshalToModel(c, modelType, &objModelPtr)
	if err != nil {
		log.Println("Update", err)
		return
	}

	/*Prepare and Validate*/
	err = server._prepareValidateModel(c, modelType, &objModelPtr)
	if err != nil {
		return
	}

	/* Perform Update */
	err = server._updateModel(c, modelType, &objModelPtr)
	if err != nil {
		return
	}

	/* Set json and return */
	server._setJSONRespWithModelStatusOk(c, modelType, &objModelPtr)
}

//Delete deletes a user
func (server *Server) Delete(c *gin.Context, modelType ModelType) {
	var objModelPtr _modelPtrs
	var err error

	userID, err := auth.ExtractTokenID(c.Request)
	if err != nil {
		c.JSON(http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	/* Find model to be deleted by ID */
	err = server._modelFindByID(c, modelType, &objModelPtr)
	if err != nil {
		return
	}

	/* Check if user exists and has the rights to delete */
	if modelType == ModelTypeUser {
		if userID != 0 && userID != uint32(objModelPtr.user.ID) {
			c.JSON(http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
			return
		}
	} else {
		objModelPtr.user, err = objModelPtr.user.FindByID(server.DB, userID)
		if err != nil {
			c.JSON(http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
			return
		}

		if objModelPtr.user.IsAdmin == false {
			c.JSON(http.StatusUnauthorized, errors.New("UnAuthorized Request"))
			return
		}
	}

	/* Perform Delete */
	objID, err := server._deleteModel(c, modelType, &objModelPtr)
	if err != nil {
		return
	}

	c.Header("Entity", fmt.Sprintf("%d", objID))
	c.JSON(http.StatusNoContent, "")
}
