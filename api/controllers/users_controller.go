package controllers

import (
	"errors"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/auth"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/models"
)

//CreateUser creates a new user
func (server *Server) CreateUser(c *gin.Context) {
	server.Create(c, ModelTypeUser)
}

//GetUser gets a user by id
func (server *Server) GetUser(c *gin.Context) {
	server.GetByID(c, ModelTypeUser)
}

//UserFollowsCourse adds a course followed by user
func (server *Server) UserFollowsCourse(c *gin.Context) {
	var objModelPtr _modelPtrs

	/* Authorize User */
	userID, err := auth.ExtractTokenID(c.Request)
	if err != nil {
		log.Println("Invalid token in update: ", userID)
		log.Println("Update", err)
		c.JSON(http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	/* Find by ID */
	err = server._modelFindByID(c, ModelTypeUser, &objModelPtr)
	if err != nil {
		return
	}

	if userID != uint32(objModelPtr.user.ID) {
		c.JSON(http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}

	// Get Course ID
	courseID, err := strconv.ParseUint(c.Param("course_id"), 10, 32)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	log.Println("Course: ", courseID, " USER: ", objModelPtr.user.ID, objModelPtr.user)
	err = server.DB.Model(&(objModelPtr.user)).Association("StudyingCourses").Append(models.Course{ID: uint32(courseID)}).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusOK, objModelPtr.user)
}
