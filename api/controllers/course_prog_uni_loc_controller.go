package controllers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/models"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/utils/formaterror"
)

//CreateCourseProgramUniLocation creates a course
func (server *Server) CreateCourseProgramUniLocation(c *gin.Context) {
	countryStr := c.DefaultQuery("country", "")
	provinceStateStr := c.DefaultQuery("province_state", "")
	cityStr := c.DefaultQuery("city", "")
	universityNameStr := c.DefaultQuery("university_name", "")
	programNameStr := c.DefaultQuery("program_name", "")
	courseNameStr := c.DefaultQuery("course_name", "")
	courseCodeStr := c.DefaultQuery("course_code", "")

	log.Println(courseCodeStr)

	if len(countryStr) == 0 || len(provinceStateStr) == 0 || len(cityStr) == 0 || len(universityNameStr) == 0 ||
		len(programNameStr) == 0 || len(courseNameStr) == 0 || len(courseCodeStr) == 0 {

		c.JSON(http.StatusUnprocessableEntity, "Empty parameters not allowed")
		return
	}

	location := models.Location{}
	location.City = cityStr
	location.Country = countryStr
	location.StateProvince = provinceStateStr
	locationPtr, err := location.Save(server.DB)

	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		c.JSON(http.StatusInternalServerError, formattedError)
		return
	}

	university := models.University{}
	university.Name = universityNameStr
	university.UniversityLocationID = locationPtr.ID
	universityPtr, err := university.Save(server.DB)

	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		c.JSON(http.StatusInternalServerError, formattedError)
		return
	}

	program := models.Program{}
	program.Name = programNameStr
	program.ProgramUniversityID = universityPtr.ID
	programPtr, err := program.Save(server.DB)

	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		c.JSON(http.StatusInternalServerError, formattedError)
		return
	}

	course := models.Course{}
	course.Name = courseNameStr
	course.Code = courseCodeStr
	course.CourseProgramID = programPtr.ID
	coursePtr, err := course.Save(server.DB)

	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		c.JSON(http.StatusInternalServerError, formattedError)
		return
	}

	term := models.Term{}
	term.StartMonth = 1
	term.Year = 1
	term.TermCourseID = coursePtr.ID
	termPtr, err := term.Save(server.DB)

	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		c.JSON(http.StatusInternalServerError, formattedError)
		return
	}

	err = server.DB.Model(coursePtr).Association("Terms").Append(models.Term{ID: uint32(termPtr.ID)}).Error
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusOK, coursePtr)
}
