package controllers

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/models"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/utils/formaterror"
)

//CreateFile creates a file
func (server *Server) CreateFile(c *gin.Context) {
	file, _ := c.FormFile("file")

	courseID, err := strconv.ParseUint(c.Param("course_id"), 10, 32)
	if err != nil {
		log.Println("File Upload Failed.", err)
		return
	}

	termID, err := strconv.ParseUint(c.Param("term_id"), 10, 32)
	if err != nil {
		log.Println("File Upload Failed.", err)
		return
	}

	//fileNameOrig := "media/" + strconv.FormatUint(courseID, 10) + "/" + strconv.FormatUint(termID, 10) + "/" + file.Filename
	filePath := filepath.Join("media/", strconv.FormatUint(courseID, 10), strconv.FormatUint(termID, 10))
	os.MkdirAll(filePath, os.ModePerm)

	filePath = filepath.Join(filePath, file.Filename)

	if err := c.SaveUploadedFile(file, filePath); err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("upload file err: %s", err.Error()))
		return
	}

	fileObj := &models.File{}
	fileObj.Name = file.Filename
	fileObj.FileCourseID = uint32(courseID)
	fileObj.FileTermID = uint32(termID)
	fileObj.Path = filePath

	fileObj.Prepare()
	err = fileObj.Validate()
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, err)
		log.Println("File: ", err)
		return
	}

	fileObj, err = fileObj.Save(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		c.JSON(http.StatusInternalServerError, formattedError)
		log.Println("File: ", err)
		return
	}

	//log.Println(courseID, termID, "*", filePath)
	//c.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", file.Filename))

	c.Header("Location", fmt.Sprintf("%s%s/%d", c.Request.Host, c.Request.RequestURI, fileObj.ID))
	c.JSON(http.StatusCreated, fileObj)
}

//GetFile gets a file by id
func (server *Server) GetFile(c *gin.Context) {
	server.GetByID(c, ModelTypeFile)
}

//SendFile sends the file
func (server *Server) SendFile(c *gin.Context) {
	var file *models.File

	objID, err := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	file, err = file.FindByID(server.DB, uint32(objID))
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	c.Writer.Header().Add("Content-Disposition", fmt.Sprintf("attachment; filename=%s", file.Name))
	//fmt.Sprintf("attachment; filename=%s", filename) Downloaded file renamed
	c.Writer.Header().Add("Content-Type", "application/octet-stream")

	c.File(file.Path)
}

//DeleteFile deletes a file
func (server *Server) DeleteFile(c *gin.Context) {
	server.Delete(c, ModelTypeFile)
}
