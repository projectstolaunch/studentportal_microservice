package controllers

import (
	"gitlab.com/projectstolaunch/studentportal_microservice/api/middlewares"
)

func (s *Server) initializeRoutes() {

	/*************************************************************************************/
	/******************************** INTEGRATED *****************************************/
	/*************************************************************************************/

	// Recent routes
	s.Router.GET("recent/courses", middlewares.SetMiddlewareJSON(), s.GetRecentCourses)

	// Search routes
	s.Router.GET("/search/locations", middlewares.SetMiddlewareJSON(), s.SearchInLocations)
	s.Router.GET("/search/universities", middlewares.SetMiddlewareJSON(), s.SearchInUniversities)
	s.Router.GET("/search/programs", middlewares.SetMiddlewareJSON(), s.SearchInPrograms)
	s.Router.GET("/search/courses", middlewares.SetMiddlewareJSON(), s.SearchInCourses)
	s.Router.GET("/search/matching/courses", middlewares.SetMiddlewareJSON(), s.SearchMatchingCourses)

	// Users routes
	s.Router.POST("/users", middlewares.SetMiddlewareJSON(), s.CreateUser)
	s.Router.GET("/users/:id", middlewares.SetMiddlewareJSON(), s.GetUser)
	s.Router.POST("/users/:id/:course_id", middlewares.SetMiddlewareJSON(), s.UserFollowsCourse)

	// Login Route
	s.Router.POST("/login", middlewares.SetMiddlewareJSON(), s.Login)
	//s.Router.HandleFunc("/login", middlewares.SetMiddlewareJSON(s.Login)).Methods("POST")

	// Course & Program & University & Location Add route
	s.Router.GET("/createcourse", middlewares.SetMiddlewareJSON(), s.CreateCourseProgramUniLocation)

	// Course Routes
	s.Router.GET("/courses/:id/:user_id", middlewares.SetMiddlewareJSON(), s.GetCourseForUser)
	s.Router.POST("/courses/addtutor/:id/:user_id", middlewares.SetMiddlewareJSON(), s.CourseAddTutor)

	// File routes
	s.Router.POST("/files/:course_id/:term_id/upload", s.CreateFile)
	s.Router.GET("/files/:id", middlewares.SetMiddlewareJSON(), s.GetFile)
	s.Router.GET("/files/:id/download", s.SendFile)

	// Tutorial routes
	s.Router.POST("/tutorials/create", s.CreateTutorial)

	/*************************************************************************************/
	/******************************** UNUSED *********************************************/
	/*************************************************************************************/

	// Home Route
	// s.Router.GET("/", middlewares.SetMiddlewareJSON(), s.Home)
	// s.Router.HandleFunc("/", middlewares.SetMiddlewareJSON(s.Home)).Methods("GET")

	// Users routes
	// s.Router.GET("/users", middlewares.SetMiddlewareJSON(), s.GetUsers)
	// s.Router.PUT("/users/:id", middlewares.SetMiddlewareJSON(), middlewares.SetMiddlewareAuthentication(), s.UpdateUser)
	// s.Router.DELETE("/users/:id", middlewares.SetMiddlewareAuthentication(), s.DeleteUser)

	// Location routes
	// s.Router.POST("/locations", middlewares.SetMiddlewareJSON(), s.CreateLocation)
	// s.Router.GET("/locations", middlewares.SetMiddlewareJSON(), s.GetLocations)
	// s.Router.GET("/locations/:id", middlewares.SetMiddlewareJSON(), s.GetLocation)
	// //s.Router.HandleFunc("/locations/{id}", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.UpdateL))).Methods("PUT")
	// s.Router.DELETE("/locations/:id", middlewares.SetMiddlewareAuthentication(), s.DeleteLocation)

	// University routes
	// s.Router.POST("/universities", middlewares.SetMiddlewareJSON(), s.CreateUniversity)
	// s.Router.GET("/universities", middlewares.SetMiddlewareJSON(), s.GetUniversities)
	// s.Router.GET("/universities/:id", middlewares.SetMiddlewareJSON(), s.GetUniversity)
	// s.Router.PUT("/universities/:id", middlewares.SetMiddlewareJSON(), middlewares.SetMiddlewareAuthentication(), s.UpdateUniversity)
	// s.Router.DELETE("/universities/:id", middlewares.SetMiddlewareAuthentication(), s.DeleteUniversity)

	// Program routes
	// s.Router.POST("/programs", middlewares.SetMiddlewareJSON(), s.CreateProgram)
	// s.Router.GET("/programs", middlewares.SetMiddlewareJSON(), s.GetPrograms)
	// s.Router.GET("/programs/:id", middlewares.SetMiddlewareJSON(), s.GetProgram)
	// s.Router.PUT("/programs/:id", middlewares.SetMiddlewareJSON(), middlewares.SetMiddlewareAuthentication(), s.UpdateProgram)
	// s.Router.DELETE("/programs/:id", middlewares.SetMiddlewareAuthentication(), s.DeleteProgram)

	// Course routes
	// s.Router.POST("/courses", middlewares.SetMiddlewareJSON(), s.CreateCourse)
	// s.Router.GET("/courses", middlewares.SetMiddlewareJSON(), s.GetCourses)
	// s.Router.GET("/courses/:id", middlewares.SetMiddlewareJSON(), s.GetCourse)
	// s.Router.PUT("/courses/:id", middlewares.SetMiddlewareJSON(), middlewares.SetMiddlewareAuthentication(), s.UpdateCourse)
	// s.Router.DELETE("/courses/:id", middlewares.SetMiddlewareAuthentication(), s.DeleteCourse)

	// Tutorial routes
	// s.Router.POST("/tutorials", middlewares.SetMiddlewareJSON(), s.CreateTutorial)
	// //s.Router.HandleFunc("/tutorials", middlewares.SetMiddlewareJSON(s.GetCourses)).Methods("GET")
	// s.Router.GET("/tutorials/:id", middlewares.SetMiddlewareJSON(), s.GetTutorial)
	// s.Router.PUT("/tutorials/:id", middlewares.SetMiddlewareJSON(), middlewares.SetMiddlewareAuthentication(), s.UpdateTutorial)
	// s.Router.DELETE("/tutorials/:id", middlewares.SetMiddlewareAuthentication(), s.DeleteTutorial)

	// Term routes
	// s.Router.POST("/terms", middlewares.SetMiddlewareJSON(), s.CreateTerm)
	// s.Router.GET("/terms", middlewares.SetMiddlewareJSON(), s.GetTerms)
	// s.Router.GET("/terms/:id", middlewares.SetMiddlewareJSON(), s.GetTerm)
	// s.Router.DELETE("/terms/:id", middlewares.SetMiddlewareAuthentication(), s.DeleteTerm)

}
