package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/models"
)

//SearchInLocations gets all locations
func (server *Server) SearchInLocations(c *gin.Context) {
	var err error
	var locations *[]models.Location
	var location *models.Location

	countryStr := c.DefaultQuery("country", "")
	provinceStateStr := c.DefaultQuery("province_state", "")
	cityStr := c.DefaultQuery("city", "")

	if countryStr != "" {
		locations, err = location.FindByCountry(server.DB, countryStr)
	} else if provinceStateStr != "" {
		locations, err = location.FindByProvinceState(server.DB, provinceStateStr)
	} else if cityStr != "" {
		locations, err = location.FindByCity(server.DB, cityStr)
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, locations)
}
