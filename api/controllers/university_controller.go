package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/models"
)

//SearchInUniversities gets all universities
func (server *Server) SearchInUniversities(c *gin.Context) {
	var err error
	var universities *[]models.University
	var university *models.University

	universityNameStr := c.DefaultQuery("university_name", "")

	if universityNameStr != "" {
		universities, err = university.FindByName(server.DB, universityNameStr)
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, universities)
}
