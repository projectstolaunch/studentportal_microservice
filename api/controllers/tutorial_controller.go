package controllers

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/auth"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/models"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/utils/formaterror"
)

//CreateTutorial creates a tutorial
func (server *Server) CreateTutorial(c *gin.Context) {
	var err error
	var tutorial *models.Tutorial

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		log.Println("Read from body failed", err)
		c.JSON(http.StatusUnprocessableEntity, err)
		return
	}

	err = json.Unmarshal(body, &(tutorial))
	if err != nil {
		log.Println("Json unmarshal failed\n", string(body), "\n", err)
		c.JSON(http.StatusUnprocessableEntity, err)
		return
	}

	/* Authorize User */
	userID, err := auth.ExtractTokenID(c.Request)
	if err != nil {
		log.Println("Invalid token in update: ", userID)
		log.Println("CreateTutorial", err)
		c.JSON(http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	if userID != uint32(tutorial.TutorialCreatorID) {
		log.Println("CreateTutorial", err)
		c.JSON(http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}

	tutorial.Prepare()
	err = tutorial.Validate()
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, err)
		log.Println(err)
		return
	}

	tutorial, err = tutorial.Save(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		c.JSON(http.StatusInternalServerError, formattedError)
		log.Println(err)
		return
	}

	c.JSON(http.StatusOK, tutorial)
}
