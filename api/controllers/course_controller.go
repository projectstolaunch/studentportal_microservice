package controllers

import (
	"errors"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/auth"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/models"
)

//GetRecentCourses returns 10 recent courses
func (server *Server) GetRecentCourses(c *gin.Context) {
	server.GetRecent(c, ModelTypeCourse)
}

//SearchInCourses gets all courses
func (server *Server) SearchInCourses(c *gin.Context) {
	var err error
	var courses *[]models.Course
	var course *models.Course

	// If only these 2 params provided, then search for just the course info
	courseNameStr := c.DefaultQuery("course_name", "")
	courseCodeStr := c.DefaultQuery("course_code", "")

	if courseNameStr != "" {
		courses, err = course.FindByCourseName(server.DB, courseNameStr)
	} else if courseCodeStr != "" {
		courses, err = course.FindByCourseCode(server.DB, courseCodeStr)
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, courses)
}

//SearchMatchingCourses gets all courses
func (server *Server) SearchMatchingCourses(c *gin.Context) {
	var err error

	var locations *[]models.Location
	var location *models.Location
	var locationIDs []uint32

	var universities *[]models.University
	var university *models.University
	var universityIDs []uint32

	var programs *[]models.Program
	var program *models.Program
	var programIDs []uint32

	var courses *[]models.Course
	var course *models.Course

	countryStr := c.DefaultQuery("country", "")
	provinceStateStr := c.DefaultQuery("province_state", "")
	cityStr := c.DefaultQuery("city", "")
	universityNameStr := c.DefaultQuery("university_name", "")
	programNameStr := c.DefaultQuery("program_name", "")
	courseNameStr := c.DefaultQuery("course_name", "")
	courseCodeStr := c.DefaultQuery("course_code", "")

	// Find matching location
	if countryStr != "" && provinceStateStr != "" && cityStr != "" {
		locations, err = location.FindByCountryProvinceStateCity(server.DB, countryStr, provinceStateStr, cityStr)
	} else if countryStr != "" && provinceStateStr != "" {
		locations, err = location.FindByCountryProvinceState(server.DB, countryStr, provinceStateStr)
	} else if provinceStateStr != "" && cityStr != "" {
		locations, err = location.FindByProvinceStateCity(server.DB, provinceStateStr, cityStr)
	} else if countryStr != "" && cityStr != "" {
		locations, err = location.FindByCountryCity(server.DB, countryStr, cityStr)
	} else if countryStr != "" {
		locations, err = location.FindByCountry(server.DB, countryStr)
	} else if provinceStateStr != "" {
		locations, err = location.FindByProvinceState(server.DB, countryStr)
	} else if cityStr != "" {
		locations, err = location.FindByCity(server.DB, countryStr)
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	if locations == nil {
		locations = &[]models.Location{}
	}

	if len(*locations) > 0 {
		for i := range *locations {
			locationIDs = append(locationIDs, (*locations)[i].ID)
		}
	}

	// Find university with location and matching name
	if universityNameStr != "" {
		universities, err = university.FindByNameAndLocations(server.DB, universityNameStr, locationIDs)
	} else {
		universities, err = university.FindByLocations(server.DB, locationIDs)
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	if universities == nil {
		universities = &[]models.University{}
	}

	if len(*universities) > 0 {
		for i := range *universities {
			universityIDs = append(universityIDs, (*universities)[i].ID)
		}
	}

	// Find Programs with university and matching name
	if programNameStr != "" {
		programs, err = program.FindByNameAndUniversities(server.DB, programNameStr, universityIDs)
	} else {
		programs, err = program.FindByUniversities(server.DB, universityIDs)
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	if programs == nil {
		programs = &[]models.Program{}
	}

	if len(*programs) > 0 {
		for i := range *programs {
			programIDs = append(programIDs, (*programs)[i].ID)
		}
	}

	// Find Courses with program and matching name/code or both
	if courseNameStr != "" && courseCodeStr != "" {
		courses, err = course.FindByCourseNameCodeAndPrograms(server.DB, courseNameStr, courseCodeStr, programIDs)
	} else if courseNameStr != "" {
		courses, err = course.FindByCourseNameAndPrograms(server.DB, courseNameStr, programIDs)
	} else if courseCodeStr != "" {
		courses, err = course.FindByCourseCodeAndPrograms(server.DB, courseCodeStr, programIDs)
	} else {
		courses, err = course.FindByPrograms(server.DB, programIDs)
	}

	if courses == nil {
		courses = &[]models.Course{}
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, courses)
}

//GetCourseForUser gets the course for user
func (server *Server) GetCourseForUser(c *gin.Context) {
	var objModelPtr _modelPtrs

	/* Authorize User */
	userID, err := auth.ExtractTokenID(c.Request)
	if err != nil {
		log.Println("Invalid token in update: ", userID)
		log.Println("Update", err)
		c.JSON(http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	/* Find User by ID */
	userIDParam, err := strconv.ParseUint(c.Param("user_id"), 10, 32)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	if userID != uint32(userIDParam) {
		c.JSON(http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}

	// Get Course ID
	err = server._modelFindByID(c, ModelTypeCourse, &objModelPtr)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	// Get User by UserID
	objModelPtr.user, err = objModelPtr.user.FindByID(server.DB, uint32(userID))
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}

	courses := []models.Course{}
	err = server.DB.Model(&objModelPtr.user).Related(&courses, "StudyingCourses").Error
	if err != nil {
		c.JSON(http.StatusNotFound, err)
		return
	}

	teachingCourses := []models.Course{}
	err = server.DB.Model(&objModelPtr.user).Related(&teachingCourses, "TutoringCourses").Error
	if err != nil {
		c.JSON(http.StatusNotFound, err)
		return
	}

	courses = append(courses, teachingCourses...)

	if len(courses) > 0 {
		for i := range courses {
			if courses[i].ID == objModelPtr.course.ID {
				// Get All Other Info
				err = server.DB.Model(&objModelPtr.course).Related(&objModelPtr.course.Files, "Files").Error
				if err != nil {
					c.JSON(http.StatusNotFound, err)
					return
				}

				err = server.DB.Model(&objModelPtr.course).Related(&objModelPtr.course.Tutorials, "Tutorials").Error
				if err != nil {
					c.JSON(http.StatusNotFound, err)
					return
				}

				err = server.DB.Model(&objModelPtr.course).Related(&objModelPtr.course.Tutors, "Tutors").Error
				if err != nil {
					c.JSON(http.StatusNotFound, err)
					return
				}
			}
		}
	}

	c.JSON(http.StatusOK, objModelPtr.course)
}

//CourseAddTutor gets the course for user
func (server *Server) CourseAddTutor(c *gin.Context) {
	var objModelPtr _modelPtrs

	/* Authorize User */
	userID, err := auth.ExtractTokenID(c.Request)
	if err != nil {
		log.Println("Invalid token in update: ", userID)
		log.Println("CourseAddTutor", err)
		c.JSON(http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	/* Find User by ID */
	userIDParam, err := strconv.ParseUint(c.Param("user_id"), 10, 32)
	if err != nil {
		log.Println("CourseAddTutor", err)
		c.JSON(http.StatusBadRequest, err)
		return
	}

	if userID != uint32(userIDParam) {
		log.Println("CourseAddTutor", err)
		c.JSON(http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}

	// Get Course ID
	err = server._modelFindByID(c, ModelTypeCourse, &objModelPtr)
	if err != nil {
		log.Println("CourseAddTutor", err)
		c.JSON(http.StatusBadRequest, err)
		return
	}

	// Get User by UserID
	objModelPtr.user, err = objModelPtr.user.FindByID(server.DB, uint32(userID))
	if err != nil {
		log.Println("CourseAddTutor", err)
		c.JSON(http.StatusBadRequest, err)
		return
	}

	err = server.DB.Model(&(objModelPtr.user)).Association("TutoringCourses").Append(
		models.Course{ID: uint32(objModelPtr.course.ID)}).Error
	if err != nil {
		log.Println("CourseAddTutor", err)
		c.JSON(http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusOK, objModelPtr.course)
}
