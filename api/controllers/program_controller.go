package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/projectstolaunch/studentportal_microservice/api/models"
)

//SearchInPrograms gets all programs
func (server *Server) SearchInPrograms(c *gin.Context) {
	var err error
	var programs *[]models.Program
	var program *models.Program

	programNameStr := c.DefaultQuery("program_name", "")

	if programNameStr != "" {
		programs, err = program.FindByName(server.DB, programNameStr)
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, programs)
}
