package models

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
)

//Term is the model for course term
type Term struct {
	ID         uint32 `gorm:"primary_key;auto_increment" json:"term_id"`
	StartMonth uint8  `gorm:"not null;" json:"term_start_month"`
	Year       uint16 `gorm:"not null;" json:"term_year"`

	//TermCourse   Course `gorm:"not null;" json:"term_course"`
	TermCourseID uint32 `gorm:"not null;" json:"term_course_id"`

	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

//Prepare function
func (obj *Term) Prepare() {
	obj.ID = 0
	//obj.TermCourse = Course{}
	obj.CreatedAt = time.Now()
	obj.UpdatedAt = time.Now()
}

//Validate function
func (obj *Term) Validate() error {
	if obj.StartMonth < 1 || obj.StartMonth > 12 {
		return errors.New("Required Term StartMonth")
	}
	if obj.Year < 1 {
		return errors.New("Required Term Year")
	}
	if obj.TermCourseID < 1 {
		return errors.New("Required Term Course")
	}
	return nil
}

//Save function
func (obj *Term) Save(db *gorm.DB) (*Term, error) {
	var err error
	err = db.Debug().Model(&Term{}).Create(&obj).Error
	if err != nil {
		return &Term{}, err
	}
	/*if obj.ID != 0 {
		err = db.Debug().Model(&Course{}).Where("id = ?", obj.TermCourseID).Take(&obj.TermCourse).Error
		if err != nil {
			return &Term{}, err
		}
	}*/
	return obj, nil
}

//FindByID function
func (obj *Term) FindByID(db *gorm.DB, pid uint32) (*Term, error) {
	var err error
	obj = &(Term{})

	err = db.Debug().Model(&Term{}).Where("id = ?", pid).Take(&obj).Error
	if err != nil {
		return &Term{}, err
	}
	/*if obj.ID != 0 {

		err = db.Debug().Model(&Course{}).Where("id = ?", obj.TermCourseID).Take(&obj.TermCourse).Error
		if err != nil {
			return &Term{}, err
		}

		if obj.TermCourseID != 0 {
			err = db.Debug().Model(&Program{}).Where("id = ?", obj.TermCourse.CourseProgramID).Take(&obj.TermCourse.CourseProgram).Error
			if err != nil {
				return &Term{}, err
			}

			if obj.TermCourse.CourseProgramID != 0 {
				err = db.Debug().Model(&University{}).Where("id = ?", obj.TermCourse.CourseProgram.ProgramUniversityID).Take(&obj.TermCourse.CourseProgram.ProgramUniversity).Error
				if err != nil {
					return &Term{}, err
				}

				if obj.TermCourse.CourseProgram.ProgramUniversityID != 0 {
					err = db.Debug().Model(&Location{}).Where("id = ?", obj.TermCourse.CourseProgram.ProgramUniversity.UniversityLocationID).Take(&(obj.TermCourse.CourseProgram.ProgramUniversity.UniversityLocation)).Error
					if err != nil {
						log.Println("Could not fetch Program->University->Location", err)
						return &Term{}, err
					}
				}
			}
		}
	}*/
	return obj, nil
}

//FindAll function
func (obj *Term) FindAll(db *gorm.DB) (*[]Term, error) {
	var err error
	terms := []Term{}
	err = db.Debug().Model(&Term{}).Limit(100).Find(&terms).Error
	if err != nil {
		return &[]Term{}, err
	}
	/*if len(terms) > 0 {
		for i := range terms {
			err = db.Debug().Model(&Course{}).Where("id = ?", terms[i].TermCourseID).Take(&terms[i].TermCourse).Error
			if err != nil {
				return &[]Term{}, err
			}

			if terms[i].TermCourseID != 0 {
				err = db.Debug().Model(&Program{}).Where("id = ?", terms[i].TermCourse.CourseProgramID).Take(&terms[i].TermCourse.CourseProgram).Error
				if err != nil {
					return &[]Term{}, err
				}

				if terms[i].TermCourse.CourseProgramID != 0 {
					err = db.Debug().Model(&University{}).Where("id = ?", terms[i].TermCourse.CourseProgram.ProgramUniversityID).Take(&terms[i].TermCourse.CourseProgram.ProgramUniversity).Error
					if err != nil {
						return &[]Term{}, err
					}

					if terms[i].TermCourse.CourseProgram.ProgramUniversityID != 0 {
						err = db.Debug().Model(&Location{}).Where("id = ?", terms[i].TermCourse.CourseProgram.ProgramUniversity.UniversityLocationID).Take(&(terms[i].TermCourse.CourseProgram.ProgramUniversity.UniversityLocation)).Error
						if err != nil {
							log.Println("Could not fetch Program->University->Location", err)
							return &[]Term{}, err
						}
					}
				}
			}
		}
	}*/
	return &terms, nil
}

//FindRecent function
func (obj *Term) FindRecent(db *gorm.DB) (*[]Term, error) {
	var err error
	terms := []Term{}
	err = db.Debug().Model(&Term{}).Order("created_at").Limit(100).Find(&terms).Error
	if err != nil {
		return &[]Term{}, err
	}
	/*if len(terms) > 0 {
		for i := range terms {
			err = db.Debug().Model(&Course{}).Where("id = ?", terms[i].TermCourseID).Take(&terms[i].TermCourse).Error
			if err != nil {
				return &[]Term{}, err
			}

			if terms[i].TermCourseID != 0 {
				err = db.Debug().Model(&Program{}).Where("id = ?", terms[i].TermCourse.CourseProgramID).Take(&terms[i].TermCourse.CourseProgram).Error
				if err != nil {
					return &[]Term{}, err
				}

				if terms[i].TermCourse.CourseProgramID != 0 {
					err = db.Debug().Model(&University{}).Where("id = ?", terms[i].TermCourse.CourseProgram.ProgramUniversityID).Take(&terms[i].TermCourse.CourseProgram.ProgramUniversity).Error
					if err != nil {
						return &[]Term{}, err
					}

					if terms[i].TermCourse.CourseProgram.ProgramUniversityID != 0 {
						err = db.Debug().Model(&Location{}).Where("id = ?", terms[i].TermCourse.CourseProgram.ProgramUniversity.UniversityLocationID).Take(&(terms[i].TermCourse.CourseProgram.ProgramUniversity.UniversityLocation)).Error
						if err != nil {
							log.Println("Could not fetch Program->University->Location", err)
							return &[]Term{}, err
						}
					}
				}
			}
		}
	}*/
	return &terms, nil
}

//Delete function
func (obj *Term) Delete(db *gorm.DB, pid uint64) (int64, error) {
	db = db.Debug().Model(&Term{}).Where("id = ?", pid).Take(&Term{}).Delete(&Term{})

	if db.Error != nil {
		if gorm.IsRecordNotFoundError(db.Error) {
			return 0, errors.New("Course not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}

//Update function
// func (obj *Term) Update(db *gorm.DB) (*Term, error) {
// 	var err error

// 	err = db.Debug().Model(&Tutorial{}).Where("id = ?", obj.ID).Updates(Tutorial{
// 		Title:      obj.Title,
// 		Link:       obj.Link,
// 		IsVerified: obj.IsVerified,
// 		UpdatedAt:  time.Now(),
// 	}).Error

// 	if err != nil {
// 		return &Tutorial{}, err
// 	}

// 	if obj.ID != 0 {
// 		err = db.Debug().Model(&User{}).Where("id = ?", obj.TutorialCreatorID).Take(&obj.TutorialCreator).Error
// 		if err != nil {
// 			return &Tutorial{}, err
// 		}

// 		err = db.Debug().Model(&Course{}).Where("id = ?", obj.TutorialCourseID).Take(&obj.TutorialCourse).Error
// 		if err != nil {
// 			return &Tutorial{}, err
// 		}
// 	}
// 	return obj, nil
// }
