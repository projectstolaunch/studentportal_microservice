package models

import (
	"errors"
	"html"
	"log"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

//Course model struct
type Course struct {
	ID   uint32 `gorm:"primary_key;auto_increment" json:"course_id"`
	Name string `gorm:"size:2048;not null;" json:"course_name"`
	Code string `gorm:"size:10;not null;" json:"course_code"`

	CourseProgram   Program    `gorm:"not null;" json:"course_program"`
	CourseProgramID uint32     `gorm:"not null;" json:"course_program_id"`
	Terms           []Term     `gorm:"foreignkey:TermCourseID;" json:"course_terms"`
	Files           []File     `gorm:"foreignkey:FileCourseID;" json:"course_files"`
	Tutorials       []Tutorial `gorm:"foreignkey:TutorialFileID;" json:"course_tutorials"`
	//Users           []*User    `gorm:"many2many:user_courses;" json:"course_users"`
	Tutors   []*User `gorm:"many2many:course_tutors;" json:"course_tutors"`
	Students []*User `gorm:"many2many:course_students;" json:"course_students"`

	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

//Prepare function
func (obj *Course) Prepare() {
	obj.ID = 0
	obj.Name = html.EscapeString(strings.TrimSpace(obj.Name))
	obj.Code = html.EscapeString(strings.TrimSpace(obj.Code))
	obj.CourseProgram = Program{}
	obj.CreatedAt = time.Now()
	obj.UpdatedAt = time.Now()
}

//Validate function
func (obj *Course) Validate() error {
	if obj.Name == "" {
		return errors.New("Required Course Name")
	}
	if obj.Code == "" {
		return errors.New("Required Course Code")
	}
	if obj.CourseProgramID < 1 {
		return errors.New("Required Course Program")
	}
	return nil
}

//FindIfExists function
func (obj *Course) FindIfExists(db *gorm.DB) (*Course, error) {
	var err error
	course := Course{}

	findFilters := "name = ? AND code = ? AND course_program_id = ?"

	err = db.Debug().Model(&Course{}).Where(findFilters, obj.Name, obj.Code, obj.CourseProgramID).Take(&course).Error
	if err != nil {
		return &Course{}, err
	}
	return &course, nil
}

//Save function
func (obj *Course) Save(db *gorm.DB) (*Course, error) {
	var err error

	existingCourse, err := obj.FindIfExists(db)
	if err == nil {
		return existingCourse, nil
	}

	err = db.Debug().Model(&Course{}).Create(&obj).Error
	if err != nil {
		return &Course{}, err
	}
	if obj.ID != 0 {
		err = db.Debug().Model(&Program{}).Where("id = ?", obj.CourseProgramID).Take(&obj.CourseProgram).Error
		if err != nil {
			return &Course{}, err
		}
	}
	return obj, nil
}

//FindAll function
func (obj *Course) FindAll(db *gorm.DB) (*[]Course, error) {
	var err error
	courses := []Course{}
	course := Course{}

	err = db.Debug().Model(&Course{}).Limit(100).Find(&courses).Error
	if err != nil {
		return &[]Course{}, err
	}

	err = course.FillCourseArrayWithInfo(db, &courses)
	if err != nil {
		return &[]Course{}, err
	}

	return &courses, nil
}

//FindRecent function
func (obj *Course) FindRecent(db *gorm.DB) (*[]Course, error) {
	var err error
	courses := []Course{}
	course := Course{}

	err = db.Debug().Model(&Course{}).Order("created_at").Limit(10).Find(&courses).Error
	if err != nil {
		return &[]Course{}, err
	}

	err = course.FillCourseArrayWithInfo(db, &courses)
	if err != nil {
		return &[]Course{}, err
	}

	return &courses, nil
}

//FindByID function
func (obj *Course) FindByID(db *gorm.DB, pid uint32) (*Course, error) {
	var err error
	course := Course{}

	err = db.Debug().Model(&Course{}).Where("id = ?", pid).Take(&course).Error
	if err != nil {
		return &Course{}, err
	}
	if course.ID != 0 {
		err = course.FillCourseInfo(db)
		if err != nil {
			return &Course{}, err
		}
	}
	return &course, nil
}

//FindByCourseName function
func (obj *Course) FindByCourseName(db *gorm.DB, courseNameStr string) (*[]Course, error) {
	var err error
	courses := []Course{}
	course := Course{}

	err = db.Debug().Model(&Course{}).Limit(10).Where("LOWER(name) LIKE LOWER(?)", "%"+courseNameStr+"%").Find(&courses).Error
	if err != nil {
		return &[]Course{}, err
	}

	err = course.FillCourseArrayWithInfo(db, &courses)
	if err != nil {
		return &[]Course{}, err
	}

	return &courses, nil
}

//FindByCourseCode function
func (obj *Course) FindByCourseCode(db *gorm.DB, courseCodeStr string) (*[]Course, error) {
	var err error
	courses := []Course{}
	course := Course{}

	err = db.Debug().Model(&Location{}).Limit(10).Where("LOWER(code) LIKE LOWER(?)", "%"+courseCodeStr+"%").Find(&courses).Error
	if err != nil {
		return &[]Course{}, err
	}

	err = course.FillCourseArrayWithInfo(db, &courses)
	if err != nil {
		return &[]Course{}, err
	}

	return &courses, nil
}

//Update function
func (obj *Course) Update(db *gorm.DB) (*Course, error) {
	var err error

	err = db.Debug().Model(&Course{}).Where("id = ?", obj.ID).Updates(Course{Name: obj.Name, UpdatedAt: time.Now()}).Error
	if err != nil {
		return &Course{}, err
	}
	if obj.ID != 0 {
		err = db.Debug().Model(&Program{}).Where("id = ?", obj.CourseProgramID).Take(&obj.CourseProgram).Error
		if err != nil {
			return &Course{}, err
		}
	}
	return obj, nil
}

//Delete function
func (obj *Course) Delete(db *gorm.DB, pid uint64) (int64, error) {
	db = db.Debug().Model(&Course{}).Where("id = ?", pid).Take(&Course{}).Delete(&Course{})

	if db.Error != nil {
		if gorm.IsRecordNotFoundError(db.Error) {
			return 0, errors.New("Course not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}

//FindByCourseNameCodeAndPrograms function
func (obj *Course) FindByCourseNameCodeAndPrograms(db *gorm.DB, courseNameStr string,
	courseCodeStr string, programIDs []uint32) (*[]Course, error) {

	var err error
	courses := []Course{}
	course := Course{}

	err = db.Debug().Model(&Course{}).Limit(10).Where(
		"LOWER(name) LIKE LOWER(?) AND LOWER(code) LIKE LOWER(?) AND course_program_id IN (?)",
		"%"+courseNameStr+"%", "%"+courseCodeStr+"%", programIDs).Find(&courses).Error
	if err != nil {
		return &[]Course{}, err
	}

	err = course.FillCourseArrayWithInfo(db, &courses)
	if err != nil {
		return &[]Course{}, err
	}

	return &courses, nil
}

//FindByCourseNameAndPrograms function
func (obj *Course) FindByCourseNameAndPrograms(db *gorm.DB, courseNameStr string, programIDs []uint32) (*[]Course, error) {

	var err error
	courses := []Course{}
	course := Course{}

	err = db.Debug().Model(&Course{}).Limit(10).Where(
		"LOWER(name) LIKE LOWER(?) AND course_program_id IN (?)", "%"+courseNameStr+"%", programIDs).Find(&courses).Error
	if err != nil {
		return &[]Course{}, err
	}

	err = course.FillCourseArrayWithInfo(db, &courses)
	if err != nil {
		return &[]Course{}, err
	}

	return &courses, nil
}

//FindByCourseCodeAndPrograms function
func (obj *Course) FindByCourseCodeAndPrograms(db *gorm.DB, courseCodeStr string, programIDs []uint32) (*[]Course, error) {

	var err error
	courses := []Course{}
	course := Course{}

	err = db.Debug().Model(&Course{}).Limit(10).Where(
		"LOWER(code) LIKE LOWER(?) AND course_program_id IN (?)", "%"+courseCodeStr+"%", programIDs).Find(&courses).Error
	if err != nil {
		return &[]Course{}, err
	}

	err = course.FillCourseArrayWithInfo(db, &courses)
	if err != nil {
		return &[]Course{}, err
	}

	return &courses, nil
}

//FindByPrograms function
func (obj *Course) FindByPrograms(db *gorm.DB, programIDs []uint32) (*[]Course, error) {

	var err error
	var course Course

	courses := []Course{}
	err = db.Debug().Model(&Course{}).Limit(10).Where("course_program_id IN (?)", programIDs).Find(&courses).Error
	if err != nil {
		return &[]Course{}, err
	}

	err = course.FillCourseArrayWithInfo(db, &courses)
	if err != nil {
		return &[]Course{}, err
	}

	return &courses, nil
}

//FillCourseArrayWithInfo fills entire course array with info
func (obj *Course) FillCourseArrayWithInfo(db *gorm.DB, courses *[]Course) error {

	if courses != nil && len(*courses) > 0 {
		for i := range *courses {
			err := (*courses)[i].FillCourseInfo(db)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

//FillCourseInfo gets course info
func (obj *Course) FillCourseInfo(db *gorm.DB) error {
	var err error

	// err = db.Debug().Model(&Term{}).Where("term_course_id = ?", obj.ID).Find(&obj.Terms).Error
	// if err != nil {
	// 	return err
	// }

	err = db.Debug().Model(&Program{}).Where("id = ?", obj.CourseProgramID).Take(&obj.CourseProgram).Error
	if err != nil {
		return err
	}

	if obj.CourseProgramID != 0 {
		err = db.Debug().Model(&University{}).Where("id = ?",
			obj.CourseProgram.ProgramUniversityID).Take(&obj.CourseProgram.ProgramUniversity).Error
		if err != nil {
			return err
		}

		if obj.CourseProgram.ProgramUniversityID != 0 {
			err = db.Debug().Model(&Location{}).Where("id = ?",
				obj.CourseProgram.ProgramUniversity.UniversityLocationID).Take(
				&(obj.CourseProgram.ProgramUniversity.UniversityLocation)).Error

			if err != nil {
				log.Println("Could not fetch Program->University->Location", err)
				return err
			}
		}
	}

	return nil
}
