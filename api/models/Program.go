package models

import (
	"errors"
	"html"
	"log"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

//Program model struct
type Program struct {
	ID   uint32 `gorm:"primary_key;auto_increment" json:"program_id"`
	Name string `gorm:"size:2048;not null;" json:"program_name"`

	ProgramUniversity   University `gorm:"not null;" json:"program_university"`
	ProgramUniversityID uint32     `gorm:"not null" json:"program_university_id"`

	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

//Prepare function
func (obj *Program) Prepare() {
	obj.ID = 0
	obj.Name = html.EscapeString(strings.TrimSpace(obj.Name))
	obj.ProgramUniversity = University{}
	obj.CreatedAt = time.Now()
	obj.UpdatedAt = time.Now()
}

//Validate function
func (obj *Program) Validate() error {
	if obj.Name == "" {
		return errors.New("Required Program Name")
	}
	if obj.ProgramUniversityID < 1 {
		return errors.New("Required Program University")
	}
	return nil
}

//FindIfExists function
func (obj *Program) FindIfExists(db *gorm.DB) (*Program, error) {
	var err error
	program := Program{}

	findFilters := "name = ? AND program_university_id = ?"

	err = db.Debug().Model(&Program{}).Where(findFilters, obj.Name, obj.ProgramUniversityID).Take(&program).Error
	if err != nil {
		return &Program{}, err
	}
	return &program, nil
}

//Save function
func (obj *Program) Save(db *gorm.DB) (*Program, error) {
	var err error

	existingProgram, err := obj.FindIfExists(db)
	if err == nil {
		return existingProgram, nil
	}

	err = db.Debug().Model(&Program{}).Create(&obj).Error
	if err != nil {
		return &Program{}, err
	}
	if obj.ID != 0 {
		err = db.Debug().Model(&Program{}).Where("id = ?", obj.ProgramUniversityID).Take(&obj.ProgramUniversity).Error
		if err != nil {
			return &Program{}, err
		}
	}
	return obj, nil
}

//FindAll function
func (obj *Program) FindAll(db *gorm.DB) (*[]Program, error) {
	var err error
	programs := []Program{}
	err = db.Debug().Model(&Program{}).Limit(100).Find(&programs).Error
	if err != nil {
		return &[]Program{}, err
	}
	if len(programs) > 0 {
		for i := range programs {
			err := db.Debug().Model(&University{}).Where("id = ?", programs[i].ProgramUniversityID).Take(&programs[i].ProgramUniversity).Error
			if err != nil {
				return &[]Program{}, err
			}

			if programs[i].ProgramUniversityID != 0 {
				err = db.Debug().Model(&Location{}).Where("id = ?", programs[i].ProgramUniversity.UniversityLocationID).Take(&(programs[i].ProgramUniversity.UniversityLocation)).Error
				if err != nil {
					log.Println("Could not fetch Program->University->Location", err)
					return &[]Program{}, err
				}
			}
		}
	}
	return &programs, nil
}

//FindRecent function
func (obj *Program) FindRecent(db *gorm.DB) (*[]Program, error) {
	var err error
	programs := []Program{}
	err = db.Debug().Model(&Program{}).Order("created_at").Limit(100).Find(&programs).Error
	if err != nil {
		return &[]Program{}, err
	}
	if len(programs) > 0 {
		for i := range programs {
			err := db.Debug().Model(&University{}).Where("id = ?", programs[i].ProgramUniversityID).Take(&programs[i].ProgramUniversity).Error
			if err != nil {
				return &[]Program{}, err
			}

			if programs[i].ProgramUniversityID != 0 {
				err = db.Debug().Model(&Location{}).Where("id = ?", programs[i].ProgramUniversity.UniversityLocationID).Take(&(programs[i].ProgramUniversity.UniversityLocation)).Error
				if err != nil {
					log.Println("Could not fetch Program->University->Location", err)
					return &[]Program{}, err
				}
			}
		}
	}
	return &programs, nil
}

//FindByID function
func (obj *Program) FindByID(db *gorm.DB, pid uint32) (*Program, error) {
	var err error
	obj = &(Program{})

	err = db.Debug().Model(&Program{}).Where("id = ?", pid).Take(&obj).Error
	if err != nil {
		return &Program{}, err
	}
	if obj.ID != 0 {
		err = db.Debug().Model(&University{}).Where("id = ?", obj.ProgramUniversityID).Take(&obj.ProgramUniversity).Error
		if err != nil {
			return &Program{}, err
		}

		if obj.ProgramUniversityID != 0 {
			err = db.Debug().Model(&Location{}).Where("id = ?", obj.ProgramUniversity.UniversityLocationID).Take(&(obj.ProgramUniversity.UniversityLocation)).Error
			if err != nil {
				log.Println("Could not fetch Program->University->Location", err)
				return &Program{}, err
			}
		}
	}
	return obj, nil
}

//FindByName function
func (obj *Program) FindByName(db *gorm.DB, programNameStr string) (*[]Program, error) {
	var err error
	programs := []Program{}
	err = db.Debug().Model(&Program{}).Limit(10).Where("LOWER(name) LIKE LOWER(?)", "%"+programNameStr+"%").Find(&programs).Error
	if err != nil {
		return &[]Program{}, err
	}
	return &programs, nil
}

//FindByNameAndUniversities function
func (obj *Program) FindByNameAndUniversities(db *gorm.DB, programNameStr string, universityIDs []uint32) (*[]Program, error) {
	var err error
	programs := []Program{}
	err = db.Debug().Model(&Program{}).Limit(10).Where("LOWER(name) LIKE LOWER(?) AND program_university_id IN (?)",
		"%"+programNameStr+"%", universityIDs).Find(&programs).Error
	if err != nil {
		return &[]Program{}, err
	}
	return &programs, nil
}

//FindByUniversities function
func (obj *Program) FindByUniversities(db *gorm.DB, universityIDs []uint32) (*[]Program, error) {
	var err error
	programs := []Program{}
	err = db.Debug().Model(&Program{}).Limit(10).Where("program_university_id IN (?)", universityIDs).Find(&programs).Error
	if err != nil {
		return &[]Program{}, err
	}
	return &programs, nil
}

//Update function
func (obj *Program) Update(db *gorm.DB) (*Program, error) {
	var err error

	err = db.Debug().Model(&Program{}).Where("id = ?", obj.ID).Updates(Program{Name: obj.Name, UpdatedAt: time.Now()}).Error
	if err != nil {
		return &Program{}, err
	}
	if obj.ID != 0 {
		err = db.Debug().Model(&University{}).Where("id = ?", obj.ProgramUniversityID).Take(&obj.ProgramUniversity).Error
		if err != nil {
			return &Program{}, err
		}

		if obj.ProgramUniversityID != 0 {
			err = db.Debug().Model(&Location{}).Where("id = ?", obj.ProgramUniversity.UniversityLocationID).Take(&(obj.ProgramUniversity.UniversityLocation)).Error
			if err != nil {
				log.Println("Could not fetch Program->University->Location", err)
				return &Program{}, err
			}
		}
	}
	return obj, nil
}

//Delete function
func (obj *Program) Delete(db *gorm.DB, pid uint64) (int64, error) {
	db = db.Debug().Model(&Program{}).Where("id = ?", pid).Take(&Program{}).Delete(&Program{})

	if db.Error != nil {
		if gorm.IsRecordNotFoundError(db.Error) {
			return 0, errors.New("Program not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
