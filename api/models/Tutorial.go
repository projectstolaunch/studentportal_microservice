package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

//Tutorial model struct
type Tutorial struct {
	ID         uint32 `gorm:"primary_key;auto_increment" json:"tutorial_id"`
	Title      string `gorm:"size:1024;not null;" json:"tutorial_title"`
	Link       string `gorm:"size:2048;not null;" json:"tutorial_link"`
	IsVerified bool   `gorm:"not null;default:false;" json:"is_verified"`
	NumViews   uint64 `gorm:"not null;default:0;" json:"num_views"`
	VoteUps    uint64 `gorm:"not null;default:0;" json:"vote_ups"`
	VoteDowns  uint64 `gorm:"not null;default:0;" json:"vote_downs"`

	//TutorialCreator   User   `gorm:"not null;" json:"tutorial_creator"`
	TutorialCreatorID uint32 `gorm:"not null;" json:"tutorial_creator_id"`
	//TutorialCourse    Course `gorm:"not null;" json:"tutorial_course"`
	TutorialCourseID uint32 `gorm:"not null;" json:"tutorial_course_id"`
	TutorialTermID   uint32 `gorm:"not null;" json:"tutorial_term_id"`
	TutorialFileID   uint32 `json:"tutorial_file_id"`

	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

//Prepare function
func (obj *Tutorial) Prepare() {
	obj.ID = 0
	obj.Title = html.EscapeString(strings.TrimSpace(obj.Title))
	obj.Link = html.EscapeString(strings.TrimSpace(obj.Link))
	/*obj.TutorialCreator = User{}
	obj.TutorialCourse = Course{}*/
	obj.CreatedAt = time.Now()
	obj.UpdatedAt = time.Now()
}

//Validate function
func (obj *Tutorial) Validate() error {
	if obj.Title == "" {
		return errors.New("Required Tutorial Title")
	}
	if obj.Link == "" {
		return errors.New("Required Tutorial Link")
	}
	if obj.TutorialCreatorID < 1 {
		return errors.New("Required Tutorial TutorialCreator")
	}
	if obj.TutorialCourseID < 1 {
		return errors.New("Required Tutorial Course")
	}
	return nil
}

//Save function
func (obj *Tutorial) Save(db *gorm.DB) (*Tutorial, error) {
	var err error
	err = db.Debug().Model(&Tutorial{}).Create(&obj).Error
	if err != nil {
		return &Tutorial{}, err
	}

	return obj, nil
}

//FindByID function
func (obj *Tutorial) FindByID(db *gorm.DB, pid uint32) (*Tutorial, error) {
	var err error
	obj = &(Tutorial{})

	err = db.Debug().Model(&Tutorial{}).Where("id = ?", pid).Take(&obj).Error
	if err != nil {
		return &Tutorial{}, err
	}
	/*if obj.ID != 0 {

		err = db.Debug().Model(&User{}).Where("id = ?", obj.TutorialCreatorID).Take(&obj.TutorialCreator).Error
		if err != nil {
			return &Tutorial{}, err
		}

		err = db.Debug().Model(&Course{}).Where("id = ?", obj.TutorialCourseID).Take(&obj.TutorialCourse).Error
		if err != nil {
			return &Tutorial{}, err
		}
		if obj.TutorialCourseID != 0 {
			err = db.Debug().Model(&Program{}).Where("id = ?", obj.TutorialCourse.CourseProgramID).Take(&obj.TutorialCourse.CourseProgram).Error
			if err != nil {
				return &Tutorial{}, err
			}

			if obj.TutorialCourse.CourseProgramID != 0 {
				err = db.Debug().Model(&University{}).Where("id = ?", obj.TutorialCourse.CourseProgram.ProgramUniversityID).Take(&obj.TutorialCourse.CourseProgram.ProgramUniversity).Error
				if err != nil {
					return &Tutorial{}, err
				}

				if obj.TutorialCourse.CourseProgram.ProgramUniversityID != 0 {
					err = db.Debug().Model(&Location{}).Where("id = ?", obj.TutorialCourse.CourseProgram.ProgramUniversity.UniversityLocationID).Take(&(obj.TutorialCourse.CourseProgram.ProgramUniversity.UniversityLocation)).Error
					if err != nil {
						log.Println("Could not fetch Program->University->Location", err)
						return &Tutorial{}, err
					}
				}
			}
		}
	}*/
	return obj, nil
}

//Update function
func (obj *Tutorial) Update(db *gorm.DB) (*Tutorial, error) {
	var err error

	err = db.Debug().Model(&Tutorial{}).Where("id = ?", obj.ID).Updates(Tutorial{
		Title:      obj.Title,
		Link:       obj.Link,
		IsVerified: obj.IsVerified,
		UpdatedAt:  time.Now(),
	}).Error

	if err != nil {
		return &Tutorial{}, err
	}

	/*if obj.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", obj.TutorialCreatorID).Take(&obj.TutorialCreator).Error
		if err != nil {
			return &Tutorial{}, err
		}

		err = db.Debug().Model(&Course{}).Where("id = ?", obj.TutorialCourseID).Take(&obj.TutorialCourse).Error
		if err != nil {
			return &Tutorial{}, err
		}
	}*/
	return obj, nil
}

//Delete function
func (obj *Tutorial) Delete(db *gorm.DB, pid uint64) (int64, error) {
	db = db.Debug().Model(&Tutorial{}).Where("id = ?", pid).Take(&Tutorial{}).Delete(&Tutorial{})

	if db.Error != nil {
		if gorm.IsRecordNotFoundError(db.Error) {
			return 0, errors.New("Course not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
