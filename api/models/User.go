package models

import (
	"errors"
	"html"
	"log"
	"strings"
	"time"

	"github.com/badoux/checkmail"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// The User model struct
type User struct {
	ID         uint32 `gorm:"primary_key;auto_increment" json:"user_id"`
	Nickname   string `gorm:"size:255;not null;unique" json:"user_display_name"`
	Email      string `gorm:"size:100;not null;unique" json:"user_email"`
	Phone      string `gorm:"size:15;" json:"user_phone"`
	Password   string `gorm:"size:100;not null;" json:"password"`
	IsAdmin    bool   `gorm:"not null;default:false" json:"is_admin"`
	IsVerified bool   `gorm:"not null;default:false" json:"is_verified"`
	IsStudent  bool   `gorm:"not null;default:false" json:"is_student"`
	IsTutor    bool   `gorm:"not null;default:false" json:"is_tutor"`

	Tutorials       []Tutorial `json:"user_tutorials"`
	StudyingCourses []*Course  `gorm:"many2many:user_courses;" json:"user_studying_courses"`
	TutoringCourses []*Course  `gorm:"many2many:course_tutors;" json:"user_teaching_courses"`

	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

//Hash function to generate hash
func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

//VerifyPassword function to verify password
func VerifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

//BeforeSave function to perform functionalities before saving the record
func (u *User) BeforeSave() error {
	hashedPassword, err := Hash(u.Password)
	if err != nil {
		return err
	}
	u.Password = string(hashedPassword)
	return nil
}

//Prepare function to prepare for model
func (u *User) Prepare() {
	u.ID = 0
	u.Nickname = html.EscapeString(strings.TrimSpace(u.Nickname))
	u.Email = html.EscapeString(strings.TrimSpace(u.Email))
	u.Phone = html.EscapeString(strings.TrimSpace(u.Phone))
	u.CreatedAt = time.Now()
	u.UpdatedAt = time.Now()
}

//Validate the data
func (u *User) Validate(action string) error {
	switch strings.ToLower(action) {
	case "update":
		if u.Nickname == "" {
			return errors.New("Required Nickname")
		}
		if u.Email == "" {
			return errors.New("Required Email")
		}
		// if u.Phone == "" {
		// 	return errors.New("Required Phone")
		// }
		if err := checkmail.ValidateFormat(u.Email); err != nil {
			return errors.New("Invalid Email")
		}
		return nil

	case "login":
		if u.Password == "" {
			return errors.New("Required Password")
		}
		if u.Email == "" {
			return errors.New("Required Email")
		}
		if err := checkmail.ValidateFormat(u.Email); err != nil {
			return errors.New("Invalid Email")
		}
		return nil

	default:
		if u.Nickname == "" {
			return errors.New("Required Nickname")
		}
		if u.Password == "" {
			return errors.New("Required Password")
		}
		if u.Email == "" {
			return errors.New("Required Email")
		}
		// if u.Phone == "" {
		// 	return errors.New("Required Phone")
		// }
		if err := checkmail.ValidateFormat(u.Email); err != nil {
			return errors.New("Invalid Email")
		}
		return nil
	}
}

//Save to save the user to database
func (u *User) Save(db *gorm.DB) (*User, error) {

	var err error
	err = db.Debug().Create(&u).Error
	if err != nil {
		return &User{}, err
	}
	return u, nil
}

//FindAll function to find all users
func (u *User) FindAll(db *gorm.DB) (*[]User, error) {
	var err error
	users := []User{}
	err = db.Debug().Model(&User{}).Limit(100).Find(&users).Error
	if err != nil {
		return &[]User{}, err
	}
	if len(users) > 0 {
		for i := range users {
			err = db.Debug().Model(&Tutorial{}).Where("tutorial_creator_id = ?", users[i].ID).Find(&users[i].Tutorials).Error
			if err != nil {
				return &[]User{}, err
			}
		}
	}

	return &users, err
}

//FindRecent function to find all users
func (u *User) FindRecent(db *gorm.DB) (*[]User, error) {
	var err error
	users := []User{}
	err = db.Debug().Model(&User{}).Order("created_at").Limit(100).Find(&users).Error
	if err != nil {
		return &[]User{}, err
	}
	if len(users) > 0 {
		for i := range users {
			err = db.Debug().Model(&Tutorial{}).Where("tutorial_creator_id = ?", users[i].ID).Find(&users[i].Tutorials).Error
			if err != nil {
				return &[]User{}, err
			}
		}
	}

	return &users, err
}

//FindByID to find user by id
func (u *User) FindByID(db *gorm.DB, uid uint32) (*User, error) {
	var err error
	user := User{}
	err = db.Debug().Model(User{}).Where("id = ?", uid).Take(&user).Error
	if err != nil {
		return &User{}, err
	}
	if gorm.IsRecordNotFoundError(err) {
		return &User{}, errors.New("User Not Found")
	}

	err = db.Debug().Model(&user).Association("StudyingCourses").Find(&user.StudyingCourses).Error
	if err != nil {
		return &User{}, err
	}
	if len(user.StudyingCourses) > 0 {
		for i := range user.StudyingCourses {

			err = db.Debug().Model(&Term{}).Where("term_course_id = ?", user.StudyingCourses[i].ID).Find(&user.StudyingCourses[i].Terms).Error
			if err != nil {
				return &User{}, err
			}

			// err = db.Debug().Model(&Tutorial{}).Where("tutorial_course_id = ?", user.StudyingCourses[i].ID).Find(&user.StudyingCourses[i].Tutorials).Error
			// if err != nil {
			// 	return &User{}, err
			// }

			err := db.Debug().Model(&Program{}).Where("id = ?", user.StudyingCourses[i].CourseProgramID).Take(&user.StudyingCourses[i].CourseProgram).Error
			if err != nil {
				return &User{}, err
			}

			if user.StudyingCourses[i].CourseProgramID != 0 {
				err = db.Debug().Model(&University{}).Where("id = ?", user.StudyingCourses[i].CourseProgram.ProgramUniversityID).Take(&user.StudyingCourses[i].CourseProgram.ProgramUniversity).Error
				if err != nil {
					return &User{}, err
				}

				if user.StudyingCourses[i].CourseProgram.ProgramUniversityID != 0 {
					err = db.Debug().Model(&Location{}).Where("id = ?", user.StudyingCourses[i].CourseProgram.ProgramUniversity.UniversityLocationID).Take(&(user.StudyingCourses[i].CourseProgram.ProgramUniversity.UniversityLocation)).Error
					if err != nil {
						log.Println("Could not fetch Program->University->Location", err)
						return &User{}, err
					}
				}
			}
		}
	}

	err = db.Debug().Model(&Tutorial{}).Where("tutorial_creator_id = ?", user.ID).Find(&user.Tutorials).Error
	if err != nil {
		return &User{}, err
	}

	return &user, err
}

//Update to update the user
func (u *User) Update(db *gorm.DB, uid uint32) (*User, error) {

	// To hash the password
	err := u.BeforeSave()
	if err != nil {
		log.Fatal(err)
	}
	db = db.Debug().Model(&User{}).Where("id = ?", uid).Take(&User{}).UpdateColumns(
		map[string]interface{}{
			"password":    u.Password,
			"nickname":    u.Nickname,
			"email":       u.Email,
			"phone":       u.Phone,
			"is_admin":    u.IsAdmin,
			"is_verified": u.IsVerified,
			"is_student":  u.IsStudent,
			"is_tutor":    u.IsTutor,
			//"update_at": time.Now(),
			"updated_at": time.Now(),
		},
	)
	if db.Error != nil {
		return &User{}, db.Error
	}
	// This is the display the updated user
	err = db.Debug().Model(&User{}).Where("id = ?", uid).Take(&u).Error
	if err != nil {
		return &User{}, err
	}
	return u, nil
}

//Delete to delete the user by id
func (u *User) Delete(db *gorm.DB, uid uint32) (int64, error) {

	db = db.Debug().Model(&User{}).Where("id = ?", uid).Take(&User{}).Delete(&User{})

	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
