package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

//University model struct
type University struct {
	ID   uint32 `gorm:"primary_key;auto_increment" json:"university_id"`
	Name string `gorm:"size:2048;not null;" json:"university_name"`

	UniversityLocation   Location `gorm:"not null;" json:"university_location"`
	UniversityLocationID uint32   `gorm:"not null" json:"university_location_id"`

	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

//Prepare function
func (obj *University) Prepare() {
	obj.ID = 0
	obj.Name = html.EscapeString(strings.TrimSpace(obj.Name))
	obj.UniversityLocation = Location{}
	obj.CreatedAt = time.Now()
	obj.UpdatedAt = time.Now()
}

//Validate function
func (obj *University) Validate() error {
	if obj.Name == "" {
		return errors.New("Required University Name")
	}
	if obj.UniversityLocationID < 1 {
		return errors.New("Required University Location")
	}
	return nil
}

//FindIfExists function
func (obj *University) FindIfExists(db *gorm.DB) (*University, error) {
	var err error
	university := University{}

	findFilters := "name = ? AND university_location_id = ?"

	err = db.Debug().Model(&University{}).Where(findFilters, obj.Name, obj.UniversityLocationID).Take(&university).Error
	if err != nil {
		return &University{}, err
	}
	return &university, nil
}

//Save function
func (obj *University) Save(db *gorm.DB) (*University, error) {
	var err error

	existingUniversity, err := obj.FindIfExists(db)
	if err == nil {
		return existingUniversity, nil
	}

	err = db.Debug().Model(&University{}).Create(&obj).Error
	if err != nil {
		return &University{}, err
	}
	if obj.ID != 0 {
		err = db.Debug().Model(&Location{}).Where("id = ?", obj.UniversityLocationID).Take(&obj.UniversityLocation).Error
		if err != nil {
			return &University{}, err
		}
	}
	return obj, nil
}

//FindAll function
func (obj *University) FindAll(db *gorm.DB) (*[]University, error) {
	var err error
	universities := []University{}
	err = db.Debug().Model(&University{}).Limit(100).Find(&universities).Error
	if err != nil {
		return &[]University{}, err
	}
	if len(universities) > 0 {
		for i := range universities {
			err := db.Debug().Model(&Location{}).Where("id = ?", universities[i].UniversityLocationID).Take(&universities[i].UniversityLocation).Error
			if err != nil {
				return &[]University{}, err
			}
		}
	}
	return &universities, nil
}

//FindRecent function
func (obj *University) FindRecent(db *gorm.DB) (*[]University, error) {
	var err error
	universities := []University{}
	err = db.Debug().Model(&University{}).Order("created_at").Limit(100).Find(&universities).Error
	if err != nil {
		return &[]University{}, err
	}
	if len(universities) > 0 {
		for i := range universities {
			err := db.Debug().Model(&Location{}).Where("id = ?", universities[i].UniversityLocationID).Take(&universities[i].UniversityLocation).Error
			if err != nil {
				return &[]University{}, err
			}
		}
	}
	return &universities, nil
}

//FindByID function
func (obj *University) FindByID(db *gorm.DB, pid uint32) (*University, error) {
	var err error
	obj = &(University{})

	err = db.Debug().Model(&University{}).Where("id = ?", pid).Take(&obj).Error
	if err != nil {
		return &University{}, err
	}
	if obj.ID != 0 {
		err = db.Debug().Model(&Location{}).Where("id = ?", obj.UniversityLocationID).Take(&obj.UniversityLocation).Error
		if err != nil {
			return &University{}, err
		}
	}
	return obj, nil
}

//FindByName function
func (obj *University) FindByName(db *gorm.DB, universityNameStr string) (*[]University, error) {
	var err error
	universities := []University{}
	err = db.Debug().Model(&University{}).Limit(10).Where("LOWER(name) LIKE LOWER(?)", "%"+universityNameStr+"%").Find(&universities).Error
	if err != nil {
		return &[]University{}, err
	}
	return &universities, nil
}

//FindByNameAndLocations function
func (obj *University) FindByNameAndLocations(db *gorm.DB, universityNameStr string, locationIDs []uint32) (*[]University, error) {
	var err error
	universities := []University{}
	err = db.Debug().Model(&University{}).Limit(10).Where("LOWER(name) LIKE LOWER(?) AND university_location_id IN (?)",
		"%"+universityNameStr+"%", locationIDs).Find(&universities).Error
	if err != nil {
		return &[]University{}, err
	}
	return &universities, nil
}

//FindByLocations function
func (obj *University) FindByLocations(db *gorm.DB, locationIDs []uint32) (*[]University, error) {
	var err error
	universities := []University{}

	err = db.Debug().Model(&University{}).Limit(10).Where("university_location_id IN (?)", locationIDs).Find(&universities).Error
	//err = db.Debug().Model(&University{}).Limit(10).Where("university_location_id IN (?)", []uint32{1, 2}).Find(&universities).Error
	if err != nil {
		return &[]University{}, err
	}
	return &universities, nil
}

//Update function
func (obj *University) Update(db *gorm.DB) (*University, error) {
	var err error

	err = db.Debug().Model(&University{}).Where("id = ?", obj.ID).Updates(University{Name: obj.Name, UpdatedAt: time.Now()}).Error
	if err != nil {
		return &University{}, err
	}
	if obj.ID != 0 {
		err = db.Debug().Model(&Location{}).Where("id = ?", obj.UniversityLocationID).Take(&obj.UniversityLocation).Error
		if err != nil {
			return &University{}, err
		}
	}
	return obj, nil
}

//Delete function
func (obj *University) Delete(db *gorm.DB, pid uint64) (int64, error) {
	db = db.Debug().Model(&University{}).Where("id = ?", pid).Take(&University{}).Delete(&University{})

	if db.Error != nil {
		if gorm.IsRecordNotFoundError(db.Error) {
			return 0, errors.New("University not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
