package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

//File model struct
type File struct {
	ID         uint32 `gorm:"primary_key;auto_increment" json:"file_id"`
	Name       string `gorm:"size:1024;not null;" json:"file_name"`
	Path       string `gorm:"size:2048;not null;" json:"file_path"`
	IsVerified bool   `gorm:"not null;" json:"is_verified"`

	FileCourseID uint32 `gorm:"not null;" json:"file_course_id"`
	FileTermID   uint32 `gorm:"not null;" json:"file_term_id"`

	Tutorials []Tutorial `gorm:"foreignkey:TutorialFileID;" json:"file_tutorials"`

	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

//Prepare function
func (obj *File) Prepare() {
	obj.ID = 0
	//obj.TermCourse = Course{}
	obj.Name = html.EscapeString(strings.TrimSpace(obj.Name))
	obj.Path = html.EscapeString(strings.TrimSpace(obj.Path))
	obj.CreatedAt = time.Now()
	obj.UpdatedAt = time.Now()
}

//Validate function
func (obj *File) Validate() error {
	if obj.FileCourseID < 1 {
		return errors.New("Required File Course")
	}
	if obj.FileTermID < 0 {
		return errors.New("Required File Term")
	}
	if obj.Name == "" {
		return errors.New("Required File Name")
	}
	return nil
}

//Save function
func (obj *File) Save(db *gorm.DB) (*File, error) {
	var err error
	err = db.Debug().Model(&Term{}).Create(&obj).Error
	if err != nil {
		return &File{}, err
	}
	return obj, nil
}

//FindByID function
func (obj *File) FindByID(db *gorm.DB, pid uint32) (*File, error) {
	var err error
	obj = &(File{})

	err = db.Debug().Model(&File{}).Where("id = ?", pid).Take(&obj).Error
	if err != nil {
		return &File{}, err
	}

	err = db.Model(&obj).Related(&obj.Tutorials, "Tutorials").Error
	if err != nil {
		return &File{}, err
	}

	return obj, nil
}

//Delete function
func (obj *File) Delete(db *gorm.DB, pid uint64) (int64, error) {
	db = db.Debug().Model(&File{}).Where("id = ?", pid).Take(&File{}).Delete(&File{})

	if db.Error != nil {
		if gorm.IsRecordNotFoundError(db.Error) {
			return 0, errors.New("File not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
