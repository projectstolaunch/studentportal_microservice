package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

//Location model struct
type Location struct {
	ID            uint32    `gorm:"primary_key;auto_increment" json:"location_id"`
	StreetNumber  uint16    `json:"street_number"`
	StreetName    string    `gorm:"size:1024;" json:"street_name"`
	City          string    `gorm:"size:1024;" json:"city"`
	StateProvince string    `gorm:"size:1024;" json:"state_province"`
	Country       string    `gorm:"size:1024;not null;" json:"country"`
	PostalCode    string    `gorm:"size:10;" json:"postal_code"`
	CreatedAt     time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt     time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

//Prepare function
func (obj *Location) Prepare() {
	obj.ID = 0
	//obj.StreetNumber = 0
	obj.StreetName = html.EscapeString(strings.TrimSpace(obj.StreetName))
	obj.City = html.EscapeString(strings.TrimSpace(obj.City))
	obj.StateProvince = html.EscapeString(strings.TrimSpace(obj.StateProvince))
	obj.Country = html.EscapeString(strings.TrimSpace(obj.Country))
	obj.PostalCode = html.EscapeString(strings.TrimSpace(obj.PostalCode))
	obj.CreatedAt = time.Now()
	obj.UpdatedAt = time.Now()
}

//Validate function
func (obj *Location) Validate() error {
	// if obj.StreetNumber < 1 {
	// 	return errors.New("Required Location Street Number")
	// }
	// if obj.StreetName == "" {
	// 	return errors.New("Required Location Street Name")
	// }
	if obj.StateProvince == "" {
		return errors.New("Required Location State")
	}
	if obj.Country == "" {
		return errors.New("Requried Location Country")
	}
	// if obj.PostalCode == "" {
	// 	return errors.New("Requried Location PostalCode")
	// }
	return nil
}

//FindIfExists function
func (obj *Location) FindIfExists(db *gorm.DB) (*Location, error) {
	var err error
	location := Location{}

	findFilters := "street_number = ? AND street_name = ? AND city = ? AND state_province = ? AND country = ? AND postal_code = ?"

	err = db.Debug().Model(&Location{}).Where(findFilters, obj.StreetNumber, obj.StreetName, obj.City, obj.StateProvince, obj.Country, obj.PostalCode).Take(&location).Error
	if err != nil {
		return &Location{}, err
	}
	return &location, nil
}

//Save function
func (obj *Location) Save(db *gorm.DB) (*Location, error) {
	var err error

	existingLocation, err := obj.FindIfExists(db)
	if err == nil {
		return existingLocation, nil
	}

	err = db.Debug().Model(&Location{}).Create(&obj).Error
	if err != nil {
		return &Location{}, err
	}
	return obj, nil
}

//FindAll function
func (obj *Location) FindAll(db *gorm.DB) (*[]Location, error) {
	var err error
	locations := []Location{}
	err = db.Debug().Model(&Location{}).Limit(100).Find(&locations).Error
	if err != nil {
		return &[]Location{}, err
	}
	return &locations, nil
}

//FindRecent function
func (obj *Location) FindRecent(db *gorm.DB) (*[]Location, error) {
	var err error
	locations := []Location{}
	err = db.Debug().Model(&Location{}).Order("created_at").Limit(100).Find(&locations).Error
	if err != nil {
		return &[]Location{}, err
	}
	return &locations, nil
}

//FindByID function
func (obj *Location) FindByID(db *gorm.DB, pid uint32) (*Location, error) {
	var err error
	location := Location{}
	err = db.Debug().Model(&Location{}).Where("id = ?", pid).Take(&location).Error
	if err != nil {
		return &Location{}, err
	}
	return &location, nil
}

//FindByCountry function
func (obj *Location) FindByCountry(db *gorm.DB, countryStr string) (*[]Location, error) {
	var err error
	locations := []Location{}
	err = db.Debug().Model(&Location{}).Limit(10).Where("LOWER(country) LIKE LOWER(?)",
		"%"+countryStr+"%").Select("id, country").Find(&locations).Error
	if err != nil {
		return &[]Location{}, err
	}
	return &locations, nil
}

//FindByProvinceState function
func (obj *Location) FindByProvinceState(db *gorm.DB, provinceStateStr string) (*[]Location, error) {
	var err error
	locations := []Location{}
	err = db.Debug().Model(&Location{}).Limit(10).Where("LOWER(state_province) LIKE LOWER(?)",
		"%"+provinceStateStr+"%").Select("id, state_province").Find(&locations).Error
	if err != nil {
		return &[]Location{}, err
	}
	return &locations, nil
}

//FindByCity function
func (obj *Location) FindByCity(db *gorm.DB, cityStr string) (*[]Location, error) {
	var err error
	locations := []Location{}
	err = db.Debug().Model(&Location{}).Limit(10).Where("LOWER(city) LIKE LOWER(?)",
		"%"+cityStr+"%").Select("id, city").Find(&locations).Error
	if err != nil {
		return &[]Location{}, err
	}
	return &locations, nil
}

//FindByCountryProvinceStateCity function
func (obj *Location) FindByCountryProvinceStateCity(db *gorm.DB, countryStr string, provinceStateStr string,
	cityStr string) (*[]Location, error) {

	var err error
	locations := []Location{}

	dbSearchFilter := "LOWER(country) LIKE LOWER(?) AND LOWER(state_province) LIKE LOWER(?) AND LOWER(city) LIKE LOWER(?)"
	err = db.Debug().Model(&Location{}).Limit(10).Where(
		dbSearchFilter, "%"+countryStr+"%", "%"+provinceStateStr+"%", "%"+cityStr+"%").Select("id").Find(&locations).Error

	if err != nil {
		return &[]Location{}, err
	}
	return &locations, nil
}

//FindByCountryProvinceState function
func (obj *Location) FindByCountryProvinceState(db *gorm.DB, countryStr string, provinceStateStr string) (*[]Location, error) {

	var err error
	locations := []Location{}

	dbSearchFilter := "LOWER(country) LIKE LOWER(?) AND LOWER(state_province) LIKE LOWER(?)"
	err = db.Debug().Model(&Location{}).Limit(10).Where(
		dbSearchFilter, "%"+countryStr+"%", "%"+provinceStateStr+"%").Select("id").Find(&locations).Error

	if err != nil {
		return &[]Location{}, err
	}
	return &locations, nil
}

//FindByProvinceStateCity function
func (obj *Location) FindByProvinceStateCity(db *gorm.DB, provinceStateStr string, cityStr string) (*[]Location, error) {

	var err error
	locations := []Location{}

	dbSearchFilter := "LOWER(state_province) LIKE LOWER(?) AND LOWER(city) LIKE LOWER(?)"
	err = db.Debug().Model(&Location{}).Limit(10).Where(
		dbSearchFilter, "%"+provinceStateStr+"%", "%"+cityStr+"%").Select("id").Find(&locations).Error

	if err != nil {
		return &[]Location{}, err
	}
	return &locations, nil
}

//FindByCountryCity function
func (obj *Location) FindByCountryCity(db *gorm.DB, countryStr string, cityStr string) (*[]Location, error) {

	var err error
	locations := []Location{}

	dbSearchFilter := "LOWER(country) LIKE LOWER(?) AND LOWER(city) LIKE LOWER(?)"
	err = db.Debug().Model(&Location{}).Limit(10).Where(
		dbSearchFilter, "%"+countryStr+"%", "%"+cityStr+"%").Select("id").Find(&locations).Error

	if err != nil {
		return &[]Location{}, err
	}
	return &locations, nil
}

// //Update function
// func (obj *Location) Update(db *gorm.DB) (*Location, error) {
// 	var err error

// 	err = db.Debug().Model(&Location{}).Where("id = ?", obj.ID).Updates(Location{Name: obj.Name, UpdatedAt: time.Now()}).Error
// 	if err != nil {
// 		return &Location{}, err
// 	}
// 	return obj, nil
// }

//Delete function
func (obj *Location) Delete(db *gorm.DB, uid uint64) (int64, error) {
	db = db.Debug().Model(&Location{}).Where("id = ?", uid).Take(&Location{}).Delete(&Location{})

	if db.Error != nil {
		if gorm.IsRecordNotFoundError(db.Error) {
			return 0, errors.New("Location not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
